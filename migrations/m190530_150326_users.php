<?php

use yii\db\Migration;
use yii\db\Schema;

class m190530_150326_users extends Migration
{
    public function safeUp()
    {
        $this->createTable('users',[
            'id'=>Schema::TYPE_PK,
            'login'=>Schema::TYPE_STRING. ' NOT NULL',
            'password'=>Schema::TYPE_STRING. ' NOT NULL'

        ]);
    }

    public function safeDown()
    {
        echo "m190530_150326_users cannot be reverted.\n";

        return false;
    }

}
