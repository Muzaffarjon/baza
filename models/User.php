<?php

namespace app\models;

use Yii;
use yii\web\IdentityInterface;
use yii\db\ActiveRecord;

class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_BLOCKED = 0;
    const STATUS_ACTIVE = 1;

    const ROLE_USER=1;
    const ROLE_ADMISION=2;
    const ROLE_ADMIN=3;
    const ROLE_SUPER_ADMIN=4;

    const SALT="HacNNdqKYmceDFHHDGBVIvcuLXJPfPUH";

    public static function tableName()
    {
        return 'users';
    }
    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return self::findOne(array('id'=>$id));
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        foreach (self::$users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }

        return null;
    }

    public static function isUserAdmin($username)
    {
        if (static::findOne(['login' => $username, 'role' => self::ROLE_ADMIN]))
            return true;

        return false;
    }

    public static function isUserOrAdmission($username,$role)
    {
        switch ($role){
            case 1:
                if (static::findOne(['login' => $username, 'role' => self::ROLE_USER]))
                    return true;
                break;
            case 2:
                if (static::findOne(['login' => $username, 'role' => self::ROLE_ADMISION]))
                    return true;
                break;
            case 4:
                if (static::findOne(['login' => $username, 'role' => self::ROLE_SUPER_ADMIN]))
                    return true;
                break;
            default: return false;
            break;
        }
    }


    public static function findByLogin($login)
    {
        //return User::find()->where(['email' => $email, 'status' => self::STATUS_ACTIVE])->one();
        return self::findOne(['login' => $login]);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return self::findOne(['login' => $username]);
//        foreach (self::$users as $user) {
//            if (strcasecmp($user['username'], $username) === 0) {
//                return new static($user);
//            }
//        }

        return null;
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        //return $this->authKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        if (Yii::$app->getSecurity()->validatePassword($password, $this->password))
            return true;
        return false;

    }

    public function getFullname()
    {
        $profile = static::find()->where(['id'=>Yii::$app->user->identity->id])->one();
        if ($profile !=null)
            return $profile->name.' '.$profile->lname.' '.$profile->oname;
        else return "Not Autorized";
        return false;
    }

    public function getLevel()
    {
        return $this->hasMany(Level::className(), ['id_user' => 'id']);
    }
}
