<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "users".
 *
 * @property integer $id
 * @property string $login
 * @property string $password
 * @property string $genauto
 * @property string $name
 * @property string $lname
 * @property string $oname
 * @property string $date_birth
 * @property string $email
 * @property integer $sex
 * @property string $passport_number
 * @property string $where_passport
 * @property string $date_passport
 * @property integer $id_city
 * @property integer $sp_int
 * @property integer $sp_unique
 * @property integer $datecreate
 * @property integer $lastupdate
 * @property integer $role
 * @property integer $status
 *
 * @property Balance[] $balances
 * @property Balance[] $balances0
 * @property City $idCity
 */
class Users extends \yii\db\ActiveRecord
{
    public $sp_unique;
    public $sp_int;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['login', 'password', 'name', 'lname', 'datecreate', 'lastupdate', 'role', 'status','genauto','email'], 'required','on'=>'registrAdmin'],
            [['sex', 'id_city', 'sp_int', 'sp_unique', 'datecreate', 'lastupdate', 'role', 'status','passport_number','where_passport','phone','date_birth','email','date_passport','login','address', 'name', 'lname'], 'required','on'=>'registrUser'],
            [['sex', 'id_city', 'datecreate', 'lastupdate', 'role', 'status','passport_number','where_passport','phone','date_birth','email','date_passport','login','address', 'name', 'lname'], 'required','on'=>'updateUser'],
            [['date_birth', 'email', 'date_passport'], 'safe'],
            [['email'], 'email'],
            ['login', 'unique'],
            ['email', 'unique'],
            ['inn', 'unique'],
            [['date_birth','date_passport'], 'date', 'format' => 'yyyy-mm-dd','on'=>'registrUser'],
            [['date_birth','date_passport'], 'date', 'format' => 'yyyy-mm-dd','on'=>'updateUser'],

            [['sex', 'id_city', 'sp_int', 'sp_unique', 'datecreate', 'lastupdate', 'role', 'status','inn'], 'integer'],
            [['login', 'name', 'lname', 'oname','phone'], 'string', 'max' => 50],
            [['password','genauto','address'], 'string', 'max' => 255],
            [['passport_number'], 'string', 'max' => 20],
            [['where_passport','email'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'login' => 'Логин',
            'password' => 'Пароль',
            'genauto' => 'Пароль',
            'address' => 'Адрес',
            'name' => 'Имя',
            'lname' => 'Фамилия',
            'oname' => 'Отчества',
            'date_birth' => 'День рождения',
            'email' => 'Электронная почта (Email)',
            'sex' => 'Пол',
            'passport_number' => 'Серия Паспорта',
            'where_passport' => 'Кем выдан паспорт',
            'date_passport' => 'Дата выдачи паспорта',
            'id_city' => 'Город или регион',
            'sp_int' => 'Sp Консультант',
            'sp_unique' => 'Sp Инвестора',
            'phone' => 'Мобильный телефон',
            'inn' => 'ИНН',
            'datecreate' => 'Datecreate',
            'lastupdate' => 'Lastupdate',
            'role' => 'Роль в системе',
            'status' => 'Status',
            'fio' => 'Ф.И.О',
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBalances0()
    {
        return $this->hasMany(Balance::className(), ['id_kassir' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCity()
    {
        return $this->hasOne(City::className(), ['id' => 'id_city']);
    }

    public function getLevel()
    {
        return $this->hasMany(Level::className(), ['id_user' => 'id']);
    }
}
