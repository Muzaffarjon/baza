<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Deals".
 *
 * @property integer $id
 * @property integer $id_level
 * @property integer $deal_id
 * @property integer $datecreate
 * @property integer $lastupdate
 * @property integer $status
 *
 * @property Level $idLevel
 * @property Balance[] $balances
 */
class Deals extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Deals';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_level', 'deal_id', 'datecreate', 'lastupdate', 'status'], 'required'],
            [['id_level', 'deal_id', 'datecreate', 'lastupdate', 'status'], 'integer'],
            [['id_level'], 'exist', 'skipOnError' => true, 'targetClass' => Level::className(), 'targetAttribute' => ['id_level' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_level' => 'Id Level',
            'deal_id' => 'Deal ID',
            'datecreate' => 'Datecreate',
            'lastupdate' => 'Lastupdate',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdLevel()
    {
        return $this->hasOne(Level::className(), ['id' => 'id_level']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBalances()
    {
        return $this->hasMany(Balance::className(), ['deals_id' => 'id']);
    }
}
