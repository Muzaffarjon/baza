<?php
/**
 * Created by PhpStorm.
 * User: RekstaR
 * Date: 23.08.18
 * Time: 12:28
 */

namespace app\models;
use yii\helpers\Url;
use yii\helpers\Html;
use Yii;
use yii\widgets\LinkPager;


class Control {

    public static function getSlider()
    {
        $slides = Slider::find()->where(['enable' => 1])->all();
        $html = "";
        foreach ($slides as $slider) {
            $html .= '<div>';
            $html .= '<img data-u="image" src="'.\Yii::$app->request->baseUrl.$slider->src.'" />';
            $html .= "<article><h1>$slider->title</h1><p>$slider->text</p></article>";
            //$html .= '<div class="caption">';
            //$html .= '<div class="name">Joy’s</div>';
            //$html .= '<a href="" target="_blank" rel="noopener noreferrer" tabindex="-1">Farmhouse Living Room ›</a>';
            //$html .= '</div>';
            $html .= '</div>';
        }
        return $html;
    }

    public static function getRetrunPage(){
        if(!empty(\Yii::$app->user->identity)){
            switch(\Yii::$app->user->identity->role){
                case 1: return ['site/index'];
                break;
                case 2: return ['designer/index'];
                break;
                case 3: return ['admin/index'];
                break;
            }
        }
    }

    public static function getMenuList($case){
        if(!empty(\Yii::$app->user->identity)){
            $menu=[];
            switch ($case) {
                case 1:
                    $menu=[
                        ['label' => '<i class="fa fa-list-alt"></i>Добавить участника', 'url' => ['users/create']],
                        ['label' => '<i class="fa  fa-columns"></i>Мои приглашения', 'url' => ['users/invited']]
                    ];
                    break;
                case 2:
                    $menu=[
                        ['label' => '<i class="fa fa-list-alt"></i>Главная', 'url' => ['users/account']],
                        ['label' => '<i class="fa  fa-user"></i>Оплаты', 'url' => ['users/payments']],
                        //['label' => '<i class="fa  fa-book"></i>Регионы', 'url' => ['users/regions']],
                        ['label' => '<i class="fa  fa-eye"></i>Мои приемы', 'url' => ['users/myrecieve']],
                    ];
                    break;
                case 3:
                $menu=[
                    ['label' => '<i class="fa fa-list-alt"></i>Главная', 'url' => ['users/account']],
                    ['label' => '<i class="fa  fa-users"></i>Пользователи', 'url' => ['users/index']],
                    ['label' => '<i class="fa  fa-columns"></i>Проверка баланса', 'url' => ['users/balance']],
                    ['label' => '<i class="fa  fa-shopping-cart"></i>Регионы', 'url' => ['users/region']],
                    ['label' => '<i class="fa  fa-user"></i>Оплата', 'url' => ['users/payments']],
                    //['label' => '<i class="fa  fa-user"></i>Должники', 'url' => ['users/depted']],
                    ['label' => '<i class="fa  fa-user"></i>Отчеты', 'url' => ['users/reports']],
                ];
                    break;
                    case 4:
                $menu=[
                    ['label' => '<i class="fa fa-list-alt"></i>Главная', 'url' => ['users/account']],
                    ['label' => '<i class="fa  fa-users"></i>Пользователи', 'url' => ['users/index']],
                    ['label' => '<i class="fa  fa-columns"></i>Проверка баланса', 'url' => ['users/balance']],
                    ['label' => '<i class="fa  fa-shopping-cart"></i>Регионы', 'url' => ['users/region']],
                    ['label' => '<i class="fa  fa-dollar-sign"></i>Проверка оплаты', 'url' => ['users/payments']],
                    //['label' => '<i class="fa  fa-user"></i>Должники', 'url' => ['users/depted']],
                    ['label' => '<i class="fa  fa-list-ol"></i>Отчеты', 'url' => ['users/reports']],
                ];
                    break;
                
       
            }
            return $menu;
        }
    }


    public static function getUserType($case){
        if(!empty(\Yii::$app->user->identity)){
            $menu="";
            switch ($case) {
                case 1:
                    $menu="Презентатор";
                    break;
                case 2:
                    $menu="Кассир";
                   break;
                case 3:
                    $menu="Админ";
                   break;
                case 4:
                    $menu="Супер Админ";
                   break;
            }
            return $menu;
        }
    }

    public static function getScenario($case){
        if(!empty(\Yii::$app->user->identity)){
            $menu="";
            switch ($case) {
                case 1:
                    $menu="registrUser";
                    break;
                case 2:
                    $menu="";
                   break;
                case 3:
                    $menu="registrAdmin";
                   break;
                case 4:
                    $menu="";
                   break;
                default:
                    $menu="registrAdmin";
                    break;
            }
            return $menu;
        }
    }


    public static function getUrl($url)
    {
        if (stripos($url, '?') === false) {
            return $url;
        } else {
            $expl = explode('?', $url);
            return $expl[0];
        }
    }

    public static function sendEmail($to,$subject,$password){

        //if(\Yii::$app->mailer->compose(['user'=>$to,'activate'=>$to->setActivateCode()])
        if(\Yii::$app->mailer->compose()
           ->setFrom(\Yii::$app->params['supportEmail'])
                   ->setTo($to->email) // кому отправляем - реальный адрес куда придёт письмо формата asdf @asdf.com
                   ->setSubject($subject) // тема письма
                   ->setTextBody("Hello, ".Html::encode($to->name.' '.$to->fname)) // текст письма без HTML
                   ->setHtmlBody(self::getBodyHtml(Html::encode($to->name.' '.$to->fname),$to->email,$password,$to->setActivateCode()))
                   ->send()===true) return true;
            return false;

    }   
    public static function sendEmailReset($to,$subject,$code){

        //if(\Yii::$app->mailer->compose(['user'=>$to,'activate'=>$to->setActivateCode()])
        if(\Yii::$app->mailer->compose()
           ->setFrom(\Yii::$app->params['supportEmail'])
                   ->setTo($to->email) // кому отправляем - реальный адрес куда придёт письмо формата asdf @asdf.com
                   ->setSubject($subject) // тема письма
                   ->setTextBody("Hello, ".Html::encode($to->name.' '.$to->fname)) // текст письма без HTML
                   ->setHtmlBody(self::getBodyHtmlforReset(Html::encode($to->name.' '.$to->fname),$code))
                   ->send()===true) return true;
            return false;

    }   

    public static function randomPassword() {
    $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < 8; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
    }

    public static function getMoneyFor($case){
        switch($case){
            case 1: return '1000';
            break;
            case 2: return '500';
            break;
            case 3: return '500';
            break;
            case 4: return '500';
            break;
        }
    }


    public static function getDateFrom($date){

        $exp=explode('.',$date);

        return $exp[2].'-'.$exp[1].'-'.$exp[0];
    }

    public static function getCountClients(){
        $clients=Users::findAll(['role'=>0]);
        return count($clients);
    }
    public static function getCountDeals(){
        $clients=Balance::findAll(['status'=>1]);
        return count($clients);
    }

    public static function getCountBalance($id_city,$money=null){
        if(!is_null($money))
        $command = Yii::$app->db->createCommand("SELECT SUM(money) FROM balance b,deals d,`level` l,users u WHERE b.deals_id=d.id AND d.id_level=l.id AND l.id_user=u.id AND u.id_city=".$id_city);
        else
            $command = Yii::$app->db->createCommand("SELECT COUNT(b.id) FROM balance b,deals d,`level` l,users u WHERE b.deals_id=d.id AND d.id_level=l.id AND l.id_user=u.id AND u.id_city=".$id_city);
        $sum = $command->queryScalar();

        if($sum!=null)
            return $sum;
        return 0;
    }

    public static function checkForBalance($id_user){
        $deals=Balance::find()->where(['deals_id'=>$id_user,'status'=>1])->one();
        if(count($deals)==0)
            return 0;
        else return 1;
    }

    public static function checkBalance($id_user){
        $command = Yii::$app->db->createCommand("SELECT sum(money) FROM balance WHERE id_kassir=".$id_user);
        $sum = $command->queryScalar();

        if($sum!=null)
            return $sum;
        return 0;
    }

    public static function getMoney($level=null){
        if(!is_null($level)){
            $command = Yii::$app->db->createCommand("SELECT SUM(money) FROM balance,Deals,`level` WHERE balance.`status`=1 AND `level`.`level`=".$level." AND balance.deals_id=Deals.id AND Deals.id_level=`level`.id");
        }else
        $command = Yii::$app->db->createCommand("SELECT sum(money) FROM balance WHERE status=1");
        $sum = $command->queryScalar();
        if($sum!=null)
        return $sum;
        return 0;
    }

    public static function getDealsByLevel($level){
        $command = Yii::$app->db->createCommand("SELECT COUNT(Deals.id) FROM Deals,`level` WHERE `level`.`level`=".$level." AND Deals.id_level=`level`.id");
        $count = $command->queryScalar();
        if($count!=null)
            return $count;
        return 0;
    }

    public static function getDepted(){
        $query = Deals::find()->alias('t1')->where(['status'=>1])->andWhere(['exists', Balance::find()->alias('t2')->select('deals_id')->andWhere('t2.deals_id = t1.id')])->orderBy(['datecreate' => SORT_DESC]);

    }

    public static function checkUserId($id,$sp_inv,$sp_uniq){
        $level=Level::find()->where(['id_user'=>$id])->orderBy(['datecreate'=>SORT_DESC])->one();

        if(empty($level)){
            if($sp_inv==0)
            self::setLevel($id,0,$sp_inv,$sp_uniq);
            else
            self::setLevel($id,1,$sp_inv,$sp_uniq);
            return true;
        }else{
            if($level->level==4){

            }else {
                self::setLevel($id,$level->level+1,$sp_inv,$sp_uniq);
                var_dump($id.' '.$sp_inv.' '.($level->level+1).' '.$sp_uniq);
                return true;
            }
        }
        return false;
    }

    public static function setLevel($id,$level,$sp_inv,$sp_uniq){
        $lev=new Level();
        $lev->id_user=$id;
        $lev->level=$level;
        $lev->sp_int=$sp_inv;
        $lev->sp_unique=$sp_uniq;
        $lev->datecreate=time();
        if($lev->save(false))
        return 1;
        else
            return 0;
    }

    public static function getSpUnique(){
        if(Yii::$app->user->identity->role==1){
            $level=Level::find()->where(['id_user'=>Yii::$app->user->identity->getId()])->one();
            return $level->sp_unique;
        }
    }

    public static function getUserLike($name,$lname,$oname,$inn,$birthday){
        $query = 'SELECT * FROM users WHERE `name` LIKE "'.$name.'" AND `lname` LIKE "'.$lname.'" AND oname LIKE "'.$oname.'" AND date_birth LIKE "'.self::getDateFrom($birthday).'" AND inn LIKE "'.$inn.'"';
        return Users::findBySql($query)->one();
        //return 2123;
    }

    public static function checkDeals($id_level){
        $deals=Deals::find()->where(['id_level'=>$id_level])->one();
        return count($deals);
    }

    public static function getDate($date){
        $exp=explode('-',$date);
        return $exp[2].'.'.$exp[1].'.'.$exp[0];
    }

    public static function getCountDeptors(){
        $deals = \app\models\Deals::find()->alias('t1')->where(['status'=>1])->andWhere(['not exists', \app\models\Balance::find()->alias('t2')->select('deals_id')->andWhere('t2.deals_id = t1.id')])->all();
      return count($deals);
    }

    public static function getInvited($sp_int){
        return Level::find()->where(['sp_int'=>$sp_int])->count();
    }

    public static function getInvitedLevel($sp_int){
        $invited=Level::find()->where(['sp_int'=>$sp_int])->one();
        return $invited->idUser->id;
    }

    public static function getAllDeptors($sp_int){
        $query='SELECT t1.*,`level`.`id` AS pid,`level`.`sp_int` FROM deals AS t1,`level` WHERE NOT EXISTS (SELECT deals_id FROM balance AS t2 WHERE t2.deals_id = t1.id) AND t1.id_level=`level`.`id` AND `level`.`sp_int`="'.$sp_int.'" ORDER BY t1.datecreate DESC;';
        return Deals::findBySql($query)->all();
    }




} 