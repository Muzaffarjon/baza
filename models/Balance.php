<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "balance".
 *
 * @property integer $id
 * @property integer $deals_id
 * @property integer $money
 * @property integer $id_kassir
 * @property integer $level
 * @property integer $datecreate
 * @property integer $lastupdate
 * @property integer $status
 *
 * @property Users $idKassir
 * @property Deals $deals
 */
class Balance extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'balance';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['deals_id', 'money', 'id_kassir', 'datecreate', 'lastupdate', 'status'], 'required'],
            [['deals_id', 'money', 'id_kassir', 'datecreate', 'lastupdate', 'status'], 'integer'],
            [['id_kassir'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['id_kassir' => 'id']],
            [['deals_id'], 'exist', 'skipOnError' => true, 'targetClass' => Deals::className(), 'targetAttribute' => ['deals_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'deals_id' => 'Deals ID',
            'money' => 'Money',
            'id_kassir' => 'Id Kassir',
            'datecreate' => 'Datecreate',
            'lastupdate' => 'Lastupdate',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdKassir()
    {
        return $this->hasOne(Users::className(), ['id' => 'id_kassir']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeals()
    {
        return $this->hasOne(Deals::className(), ['id' => 'deals_id']);
    }


}
