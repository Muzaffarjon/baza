<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "level".
 *
 * @property integer $id
 * @property integer $id_user
 * @property integer $level
 * @property integer $sp_int
 * @property integer $sp_unique
 * @property integer $datecreate
 *
 * @property Deals[] $deals
 * @property Users $idUser
 */
class Level extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'level';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user', 'level', 'sp_int', 'sp_unique', 'datecreate'], 'required', 'on'=>'leveling'],
            [['sp_int', 'sp_unique'], 'required', 'on'=>'invite'],
            [['id_user', 'level', 'datecreate'], 'integer'],
            [['sp_int', 'sp_unique'],'integer', 'min'=>5, 'on'=>'invite'],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['id_user' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user' => 'Id User',
            'level' => 'Level',
            'sp_int' => 'Sp Консультанта',
            'sp_unique' => 'Sp Индивидуальный',
            'datecreate' => 'Datecreate',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeals()
    {
        return $this->hasMany(Deals::className(), ['id_level' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'id_user']);
    }
}
