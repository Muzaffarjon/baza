<?php

namespace app\controllers;

use app\models\Balance;
use app\models\City;
use app\models\Control;
use app\models\Date;
use app\models\Deals;
use app\models\Level;
use app\models\User;
use Yii;
use app\models\Users;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UsersController implements the CRUD actions for Users model.
 */
class UsersController extends Controller
{
    public $layout = "admin";

    /**
     * @inheritdoc
     */

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['create','index','update','view','account','agreement','balance','invited','gave','depted','payments','kassir','reports','pay','deal','direct','add','unpay','myrecieve','regions','region'],
                'rules' => [
                    [
                        'actions' => ['create','index','update','view','account','agreement','gave','balance','depted','payments','kassir','reports','pay','deals','add','region'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return \app\models\User::isUserAdmin(Yii::$app->user->identity->login);
                        }
                    ],
                    [
                        'actions' => ['create','index','upd','view','account','agreement','gave','balance','depted','payments','kassir','reports','pay','index','deals','add','region'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return \app\models\User::isUserOrAdmission(Yii::$app->user->identity->login,4);
                        }
                    ],
                    [
                        'actions' => ['view','account','gave','balance','payments','pay','unpay','myrecieve','regions'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return \app\models\User::isUserOrAdmission(Yii::$app->user->identity->login,2);
                        }
                    ],
                    [
                        'actions' => ['invited','account','create','view','update','deal','agreement','direct'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return \app\models\User::isUserOrAdmission(Yii::$app->user->identity->login,1);
                        }
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }


    /**
     * Lists all Users models.
     * @return mixed
     */

    public function actionDeals($id){
        $users=Users::findOne($id);

       return $this->render('deals',['user'=>$users]);
    }

    public function actionRegion(){
        $city=City::find();

        $dataProvider = new ActiveDataProvider([
            'query' =>$city
        ]);

        return $this->render('regions', [
            'dataProvider' => $dataProvider,
        ]);

    }

    public function actionIndex($type=null)
    {

        if(!is_null($type))
            $query=Users::find()->where(['not in', 'id', Yii::$app->user->identity->getId()])->andWhere(['not in', 'role', [4,3]])->andWhere(['role'=>$type])->orderBy(['datecreate'=>SORT_DESC]);
        else
            $query=Users::find()->where(['not in', 'id', Yii::$app->user->identity->getId()])->andWhere(['not in', 'role', [4,3]])->orderBy(['datecreate'=>SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' =>$query
            ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionRegions()
    {
        $this->render('regions');
    }



    public function actionAgreement($id)
    {

        $model = new Deals();
        $model->id_level = $id;
        $model->deal_id = rand(10000000, 1000000000);
        $model->datecreate = time();
        $model->lastupdate = time();
        $model->status = 1;
        $find = Deals::find()->where(['id_level' => $id])->one();
        if (empty($find)) {
            if ($model->save())
                return $this->redirect(['users/invited']);
        } else return $this->redirect(['users/invited']);
    }

    public function actionBalance()
    {
        $balance=Balance::findAll(['status'=>1]);
        $bal=Balance::findBySql("SELECT SUM(money) as money,id_kassir FROM balance GROUP BY id_kassir ORDER BY money DESC")->all();
        return $this->render('balance',['deals'=>$balance,'bal'=>$bal]);
    }

    public function actionDepted()
    {
        return $this->render('depted');
    }

    public function actionKassir()
    {
        $kassir=Users::find()->where(['role'=>2])->all();
        return $this->render('kassir',['kassir'=>$kassir]);
    }

    public function actionMyrecieve()
    {
        $deals=Balance::find()->where(['id_kassir'=>Yii::$app->user->identity->getId()])->all();
        return $this->render('recieve',['deals'=>$deals]);
    }

    public function actionPayments()
    {

        $deals = \app\models\Deals::find()->alias('t1')->where(['status'=>1])->andWhere(['not exists', \app\models\Balance::find()->alias('t2')->select('deals_id')->andWhere('t2.deals_id = t1.id')])->all();
        //var_dump($deals);
        return $this->render('payments');
    }

    public function actionPay($id)
    {
        $model = new Balance();
        $deals = $this->findDeals($id);
            $model->id_kassir = Yii::$app->user->identity->getId();
            $model->deals_id = $deals->id;
            switch ($deals->idLevel->level) {
                case 1:
                    $model->money = 1000;
                    break;
                case 2:
                    $model->money = 500;
                    break;
                case 3:
                    $model->money = 500;
                    break;
                case 4:
                    $model->money = 500;
                    break;
            }
            $model->datecreate = time();
            $model->lastupdate = time();
            $model->status = 1;
                if($model->save())
                return $this->redirect(['users/payments']);
    }

    public function actionReports($type = null)
    {
        switch ($type) {
            case 'present':

                if (isset($_GET['id'])) {
                    var_dump($_GET['id']);
                    $user = Users::findOne($_GET['id']);
                    $all = Level::find()->where(['sp_int' => $user->level->sp_unique])->all();
                    return $this->render('includes/investor', ['all' => $all]);
                } else {
                    $users = Users::find()->where(['role' => 1])->all();
                    return $this->render('includes/present', ['users' => $users]);
                }
                break;
            case 'date':
                if ((isset($_POST['date_start']) && isset($_POST['date'])) or isset($_POST['statr_date']) or isset($_POST['date_end'])) {
                    Yii::$app->session->set('date_start', $_POST['date_start']);
                    Yii::$app->session->set('date_end', $_POST['date_end']);
                    $balance = Balance::find()->where(['between', 'datecreate', strtotime(Yii::$app->session->get('date_start')), strtotime(Yii::$app->session->get('date_end'))])->orderBy(['datecreate' => SORT_DESC])->all();
                    return $this->render('includes/date', ['balance' => $balance]);
                }
                return $this->render('includes/date');
                break;
            case 'dose':
                return $this->render('includes/dose');
                break;

            case 'goo':
                if (isset($_GET['sp'])) {
                    $level = Level::find()->where(['sp_int' => $_GET['sp']])->all();
                    return $this->render('includes/presentator', ['level' => $level]);
                }
                break;
            case 'region':
                $city = City::find()->all();
                return $this->render('includes/region', ['city' => $city]);
                break;
            default:
                $users = Users::find()->where(['role' => 1])->all();
                return $this->render('includes/present', ['users' => $users]);
        }

    }


    /**
     * Displays a single Users model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionAccount()
    {

        return $this->render('dashboard');
    }

    /**
     * Creates a new Users model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Users();

        $model->scenario = "registrUser";
        $model->datecreate = time();
        $model->lastupdate = time();
        if (Yii::$app->user->identity->role == 1)
            $model->role = 0;
        $model->status = 1;

        if ($model->load(Yii::$app->request->post())) {
            $check = Control::getUserLike($model->name, $model->lname, $model->oname, $model->inn, $model->date_birth);
            if (empty($check)) {
                //var_dump($check);
                $model->login = $this->generateRandomString();
                $model->genauto = $model->login;
                $model->sp_int = Control::getSpUnique();
                $model->date_birth = Control::getDateFrom($_POST['Users']['date_birth']);
                $model->date_passport = Control::getDateFrom($_POST['Users']['date_passport']);
                $model->password = Yii::$app->getSecurity()->generatePasswordHash($model->login);


                if ($model->validate()) {

                    if ($model->save()) {
                        Control::checkUserId($model->id, $model->sp_int, $model->sp_unique);
                        return $this->redirect(['users/invited']);
                    }
                }
            } else {
                //Control::saveSession($model);
                return $this->redirect(['users/direct', 'id' => $check->id]);
            }

        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionDirect($id)
    {
        $model = $this->findModelDirect($id);
        $level=new Level();
        $level->scenario="invite";
        if ($level->load(Yii::$app->request->post())) {
           // var_dump($_POST);
            if ($level->validate()) {
                //var_dump($_POST);
                Control::checkUserId($id, $level->sp_int, $level->sp_unique);
                return $this->redirect(['users/invited']);
            }
        }

        return $this->render('direct', [
            'model' => $model,
            'level' => $level,
        ]);
    }

    public function actionAdd($type)
    {
        $model = new Users();
        if ($type == 1) {
            $model->scenario = "registrUser";
            $model->role = 1;
        } elseif ($type == 2) {
            $model->scenario = "registrAdmin";
            $model->role = 2;
        }
        if (Yii::$app->user->identity->role==4) {
            $model->role = 3;
            $model->scenario = "registrAdmin";
        }else {
            $this->redirect(['users/index']);
        }
        $model->datecreate = time();
        $model->lastupdate = time();
        $model->status = 1;

        if ($model->load(Yii::$app->request->post())) {
           // if ($type == 1) {
              //  $model->login = $this->generateRandomString();
              //  $model->genauto = $model->login;
              //  $model->password = Yii::$app->getSecurity()->generatePasswordHash($model->login);
                $model->sp_int=0;
           // } else {
             //   $model->login = $_POST['Users']['login'];
                $model->password = Yii::$app->getSecurity()->generatePasswordHash($_POST['Users']['genauto']);
          //  }
            $model->date_birth = Control::getDateFrom($_POST['Users']['date_birth']);
            $model->date_passport = Control::getDateFrom($_POST['Users']['date_passport']);


            if ($model->validate()) {
                if ($model->save()) {
                    if($type==1)
                    Control::checkUserId($model->id, $model->sp_int, $model->sp_unique);
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }
        }


        return $this->render('add', [
            'model' => $model,
            'type' => $type,
        ]);
    }

    public function balance()
    {
        $this->render('balance');
    }

    public function actionInvited()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Level::find()->where(['sp_int' => Control::getSpUnique()])->orderBy(['datecreate' => SORT_DESC]),
        ]);

        return $this->render('invited', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionUnpay($id){
        $balance=Balance::find()->where(['deals_id'=>$id,'id_kassir'=>Yii::$app->user->identity->getId()])->one();
        if($balance->delete())
            return $this->redirect(['users/payments']);
    }

    private function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function actionStatus()
    {
        if (\Yii::$app->request->isAjax) {
            $blog = Users::findOne($_POST['id']);
            $value = $_POST['value'];
            if ($value == 0)
                $blog->status = 0;
            elseif ($value == 1)
                $blog->status = 1;
            if ($blog->save())
                return 1;
            else return 0;
        }
    }

    /**
     * Updates an existing Users model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $level=new Level();
        //if($model->level->sp_int==Control::getSpUnique()){
        //var_dump($model->level->sp_int.', '.Control::getSpUnique());
        $model->scenario = "updateUser";
        $now = strtotime("-10 minutes");

            if ($model->load(Yii::$app->request->post())) {
                $model->date_birth = Control::getDateFrom($_POST['Users']['date_birth']);
                $model->date_passport = Control::getDateFrom($_POST['Users']['date_passport']);
                if($model->validate()){
                $model->save();
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }


            if($level->load(Yii::$app->request->post())){
                $level=Level::findOne(['id'=>$id,'id_user'=>Yii::$app->user->identity->getId()]);
                $level->scenario="invite";
                $level->sp_int=$_POST['Level']['sp_int'];
                $level->sp_unique=$_POST['Level']['sp_unique'];
                $level->datecreate=time();
                $level->save();
            }

           //if ($now > $model->datecreate)
            //    return $this->redirect(['users/account' ]);
           //else
                return $this->render('update', ['model' => $model,'level'=>$level]);
        //}else return $this->goBack();

    }

    public function actionUpd($id,$type)
    {
        $model = $this->findModel($id);
        if ($type == 1) {
            $model->scenario = "registrUser";
            $model->role = 1;
        } elseif ($type == 2) {
            $model->scenario = "registrAdmin";
            $model->role = 2;
        }
        if (Yii::$app->user->identity->role==4) {
            $model->role = 3;
            $model->scenario = "registrAdmin";
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

            return $this->render('upd', ['model' => $model,'type'=> $type]);


    }

    public function thisaction($model)
    {
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }
        return $this->render('update', [
            'model' => $model
        ]);
    }

    /**
     * Deletes an existing Users model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if (Yii::$app->user->identity->role == 4)
            $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Users model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Users the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */

    protected function findModelDirect($id)
    {
        if (Yii::$app->user->identity->role == 1){
            $model = Users::find()->where(['id'=>$id,'status'=>1,'role'=>0])->one();
        }else
            $model = Users::findOne($id);
        //var_dump($model);
        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModel($id)
    {
        if (Yii::$app->user->identity->role == 1){
            $query="SELECT users.*,level.sp_int,level.sp_unique,level.level FROM users,`level` WHERE users.id=".$id." AND LEVEL.sp_int='".Control::getSpUnique()."' AND users.id=LEVEL.id_user AND users.role=0";
            $model = Users::findBySql($query)->one();
        }else
            $model = Users::findOne($id);
        //var_dump($model);
        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findDeals($id)
    {

        $model = Deals::findOne($id);
        //var_dump($model);
        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
