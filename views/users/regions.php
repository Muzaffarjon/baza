<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;


/* @var $this yii\web\View */
/* @var $model app\models\Users */

$this->title = 'Печатать договор';
$this->params['breadcrumbs'][] = ['label'=>$this->title,'template'=>"<li class='list-inline-item'>{link}</li>"];
?>
<style>
    .table a{
        font-size: 19px;
        padding-right:3px;
    }
</style>
<script type="text/javascript">

    function update(id) {
        var data = $('#check_'+id).val();
        $.ajax({
            type: 'POST',
            url: "<?php echo Url::to(['users/status']); ?>",
            cache: false,
            data: {id:id,value:data},
            error: function () {

            },
            beforeSend: function () {
                $('checkbox').attr('disabled', 'disabled');
            },
            success: function (data) {
                $('checkbox').removeAttr('disabled');
                //location.reload();
                $.pjax.reload({container: '#pjax-container'});
            }
        })
    }

</script>
<div class="row">
    <div class="col-md-6 ">
        <?php Pjax::begin(['id' => 'pjax-container']); ?>
        <div class="table-responsive table-responsive table-responsive-data3" style="margin-bottom: 25px;">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'tableOptions' => [
                    'class' => 'table table-borderless table-data3'
                ],
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    //'id',
                    'city',
                    //'genauto',

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'header' => 'Actions',
                        'headerOptions' => ['style' => 'color:#337ab7'],
                        'template' => '{agree}{view}{update}',
                        'buttons' => [

                            'update' => function ($url, $model) {

                                        return Html::a('<button type="button" class="btn btn-primary m-l-10"><span class="zmdi zmdi-edit"></span></button>', $url, ['class'=>'item' ,
                                            'title' => Yii::t('app', 'update'),
                                        ]);

                            },

                        ],
                        'urlCreator' => function ($action, $model, $key, $index) {

                            if ($action === 'update') {
                                $url =Url::to(['users/cupdate','id'=>$model->id]);
                                return $url;
                            }
                        }
                    ],
                ],
            ]); ?>
        </div>
        <?php Pjax::end(); ?>
    </div>
</div>
