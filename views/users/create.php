<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Users */

$this->title = 'Создать участника';
$this->params['breadcrumbs'][] = ['label'=>$this->title,'template'=>"<li class='list-inline-item'>{link}</li>"];
?>
<div class="row">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">
                <strong class="card-title"><?= Html::encode($this->title) ?></strong>
            </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

        </div>
    </div>
</div>