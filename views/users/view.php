<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Users */

$this->title = $model->name;
if(Yii::$app->user->identity->role==3 or Yii::$app->user->identity->role==4)
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
else $this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['invited']];
$this->params['breadcrumbs'][] = ['label'=>$this->title,'template'=>"<li class='list-inline-item'>{link}</li>"];
?>

    <div class="row section__content--p30">
        <div class="col col-lg-12">
            <section class="card">
                <div class="card-body text-secondary">
                    <h1 class="m-b-40"><?= $model->name.' '.$model->lname.' '.$model->oname ?></h1>
                    <?php
                    if(Yii::$app->user->identity->role!=1){
                        ?>
                        <p>
                            <?/*= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                                'class' => 'btn btn-danger',
                                'data' => [
                                    'confirm' => 'Are you sure you want to delete this item?',
                                    'method' => 'post',
                                ],
                            ]) */?>
                        </p>
                    <?php } ?>
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            //'id',
                            'name',
                            'lname',
                            'oname',
                            [
                                'attribute'=>'date_birth',
                                'value'=>$model->date_birth,
                                'format'=>'raw'
                            ],
                            'email:email',
                            [
                                'attribute' => 'sex',
                                'format' => 'raw',
                                'value' => function ($model) {
                                    if ($model->sex == 0) {
                                        return 'Женщина'; // "x" icon in red color
                                    } else {
                                        return 'Мужчина'; // check icon
                                    }
                                },
                            ],
                            'passport_number',
                            'where_passport',
                            'date_passport',
                            [
                                'label' => 'Sp Индивидуальный',
                                'attribute' => 'sp_unique',
                                'value'=>\app\models\Control::getSpUnique(),

                            ],
                            [
                                'label' => 'Город',
                                'attribute' => 'id_city',
                                'value'=>$model->idCity->city,

                        ],
                        ],
                    ]) ?>

                </div>
            </section>
        </div>
    </div>

