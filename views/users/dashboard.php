<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Dashboard';
$this->params['breadcrumbs'][] = ['label'=>"",'template'=>"<li class='list-inline-item'>{link}</li>"];

?>
<?php if(Yii::$app->user->identity->role==1 or Yii::$app->user->identity->role==2){?>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <strong class="card-title">Добро пожаловать уважаемый <?= Yii::$app->user->identity->getFullname(); ?></strong>
            </div>
            <div class="card-body">
                <div class="alert alert-warning" role="alert">
                    <h4 class="alert-heading">Well done!</h4>
                    <p>Aww yeah, you successfully read this important alert message. This example text is going to run a bit longer so
                        that you can see how spacing within an alert works with this kind of content.</p>
                    <hr>
                    <p class="mb-0">Whenever you need to, be sure to use margin utilities to keep things nice and tidy.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<?php }elseif(Yii::$app->user->identity->role==3 or Yii::$app->user->identity->role==4){  ?>
    <section class="">
        <div class="section__content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6 col-lg-3">
                        <div class="statistic__item">
                            <h2 class="number"><?= \app\models\Control::getCountClients(); ?></h2>
                            <span class="desc">Клиенты</span>
                            <div class="icon">
                                <i class="zmdi zmdi-account-o"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-3">
                        <div class="statistic__item">
                            <h2 class="number"><?= \app\models\Control::getCountDeals(); ?></h2>
                            <span class="desc">Продажи</span>
                            <div class="icon">
                                <i class="zmdi zmdi-shopping-cart"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-3">
                        <div class="statistic__item">
                            <h2 class="number"><?= \app\models\Control::getCountDeptors(); ?></h2>
                            <span class="desc">Должники</span>
                            <div class="icon">
                                <i class="zmdi zmdi-calendar-note"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-3">
                        <div class="statistic__item">
                            <h2 class="number"><?= \app\models\Control::getMoney(); ?></h2>
                            <span class="desc">Баланс</span>
                            <div class="icon">
                                <i class="zmdi zmdi-money"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<!-- END STATISTIC-->

<section>
    <div class="section__content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-8">
                    <!-- RECENT REPORT 2-->
                    <div class="recent-report2">
                        <h3 class="title-3">recent reports</h3>
                        <div class="chart-info">
                            <div class="chart-info__left">
                                <div class="chart-note">
                                    <span class="dot dot--blue"></span>
                                    <span>products</span>
                                </div>
                                <div class="chart-note">
                                    <span class="dot dot--green"></span>
                                    <span>Services</span>
                                </div>
                            </div>
                            <div class="chart-info-right">
                                <div class="rs-select2--dark rs-select2--md m-r-10">
                                    <select class="js-select2" name="property">
                                        <option selected="selected">All Properties</option>
                                        <option value="">Products</option>
                                        <option value="">Services</option>
                                    </select>
                                    <div class="dropDownSelect2"></div>
                                </div>
                                <div class="rs-select2--dark rs-select2--sm">
                                    <select class="js-select2 au-select-dark" name="time">
                                        <option selected="selected">All Time</option>
                                        <option value="">By Month</option>
                                        <option value="">By Day</option>
                                    </select>
                                    <div class="dropDownSelect2"></div>
                                </div>
                            </div>
                        </div>
                        <div class="recent-report__chart">
                            <canvas id="recent-rep2-chart"></canvas>
                        </div>
                    </div>
                    <!-- END RECENT REPORT 2             -->
                </div>
                <div class="col-xl-4">
                    <!-- TASK PROGRESS-->
                    <div class="task-progress">
                        <h3 class="title-3">task progress</h3>
                        <div class="au-skill-container">
                            <div class="au-progress">
                                <span class="au-progress__title">Web Design</span>
                                <div class="au-progress__bar">
                                    <div class="au-progress__inner js-progressbar-simple" role="progressbar" data-transitiongoal="90">
                                        <span class="au-progress__value js-value"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="au-progress">
                                <span class="au-progress__title">HTML5/CSS3</span>
                                <div class="au-progress__bar">
                                    <div class="au-progress__inner js-progressbar-simple" role="progressbar" data-transitiongoal="85">
                                        <span class="au-progress__value js-value"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="au-progress">
                                <span class="au-progress__title">WordPress</span>
                                <div class="au-progress__bar">
                                    <div class="au-progress__inner js-progressbar-simple" role="progressbar" data-transitiongoal="95">
                                        <span class="au-progress__value js-value"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="au-progress">
                                <span class="au-progress__title">Support</span>
                                <div class="au-progress__bar">
                                    <div class="au-progress__inner js-progressbar-simple" role="progressbar" data-transitiongoal="95">
                                        <span class="au-progress__value js-value"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END TASK PROGRESS-->
                </div>
            </div>
        </div>
    </div>
</section>
<?php } ?>