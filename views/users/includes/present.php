<?php

use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\Users */

$this->title = 'Презентаторы';
$this->params['breadcrumbs'][] = ['label'=>$this->title,'template'=>"<li class='list-inline-item'>{link}</li>"];
?>
<?php Pjax::begin(['enablePushState'=>true,'timeout'=>10000,'class'=>'']); ?>
<div id="reload" class="row">
    <div class="col-md-2">
        <div class="card">
            <div class="card-header">
                <strong>Block Level Buttons </strong>
            </div>
            <div class="card-body" style="color: #fff;">
                <a type="button" href="<?= \yii\helpers\Url::to(['users/reports','type'=>'present']) ?>" class="btn btn-success btn-lg btn-block">Презентаторы</a>
                <a type="button" href="<?= \yii\helpers\Url::to(['users/reports','type'=>'date']) ?>" class="btn btn-primary btn-lg btn-block">Поиск по дате</a>

                <a type="button" href="<?= \yii\helpers\Url::to(['users/reports','type'=>'dose']) ?>" class="btn btn-primary btn-lg btn-block">Досье</a>
                <a type="button" href="<?= \yii\helpers\Url::to(['users/reports','type'=>'region']) ?>" class="btn btn-primary btn-lg btn-block">Регионы</a>
<!--                <a type="button" href="--><?//= \yii\helpers\Url::to(['users/reports','type'=>'investor']) ?><!--" class="btn btn-primary btn-lg btn-block">Sp инвестора</a>-->
            </div>
        </div>
    </div>
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">
                <strong>Block Level Buttons </strong>
                <small>Use this class
                    <code>.btn-block</code>
                </small>
            </div>
            <div class="card-body" >
                <div class="table-responsive table--no-card m-b-30">
                <table class="table table-borderless table-striped table-earning">
                    <thead>
                    <tr>
                        <th>Ф.И.О</th>
                        <th class="text-right">Телефон</th>
                        <th class="text-right">Sp Консултантов</th>
                        <th class="text-right">Invited</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    //                echo '<pre>';
                    //                var_dump($usereals);
                    //                echo '</pre>';
                    foreach ($users as $user){
                        echo "<tr>";
                        echo "<td class='text-left'><a href='".\yii\helpers\Url::to(['users/reports','type'=>'goo','sp'=>$user->level[0]->sp_unique])."' >".$user->name." ".$user->lname." ".$user->oname."</a></td>";
                        echo "<td class='text-right'>".$user->phone."</td>";
                        echo "<td class='text-right'>".$user->level[0]->sp_unique."</td>";
                        echo "<td class='text-right'>".\app\models\Control::getInvited($user->level[0]->sp_unique)."</td>";
                        echo  '</td>';
                        echo '</tr>';
                    }
                    ?>
                    </tbody>
                </table>
            </div>
            </div>
        </div>
    </div>
</div>
<?php Pjax::end(); ?>