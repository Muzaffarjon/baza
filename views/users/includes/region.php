<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\Users */

$this->title = 'Регионы';
$this->params['breadcrumbs'][] = ['label'=>$this->title,'template'=>"<li class='list-inline-item'>{link}</li>"];
?>
<?php Pjax::begin(['enablePushState'=>true,'timeout'=>10000,'class'=>'']); ?>
<div id="reload" class="row">
    <div class="col-md-2">
        <div class="card">
            <div class="card-header">
                <strong>Block Level Buttons </strong>
            </div>
            <div class="card-body" style="color: #fff;">
                <a type="button" href="<?= \yii\helpers\Url::to(['users/reports','type'=>'present']) ?>" class="btn btn-primary btn-lg btn-block">Презентаторы</a>
                <a type="button" href="<?= \yii\helpers\Url::to(['users/reports','type'=>'date']) ?>" class="btn btn-primary btn-lg btn-block">Поиск по дате</a>

                <a type="button" href="<?= \yii\helpers\Url::to(['users/reports','type'=>'dose']) ?>" class="btn btn-primary btn-lg btn-block">Досье</a>
                <a type="button" href="<?= \yii\helpers\Url::to(['users/reports','type'=>'region']) ?>" class="btn btn-success btn-lg btn-block">Регионы</a>
<!--                <a type="button" href="--><?//= \yii\helpers\Url::to(['users/reports','type'=>'investor']) ?><!--" class="btn btn-primary btn-lg btn-block">Sp инвестора</a>-->
            </div>
        </div>
    </div>
    <div class="col">
        <div class="row">
            <div class="col-lg-4">
                <h2 class="title-1 m-b-25">Договоры</h2>
                <div class="au-card au-card--bg-blue au-card-top-countries m-b-40">
                    <div class="au-card-inner">
                        <div class="table-responsive">
                            <table class="table table-top-countries">
                                <tbody>
                                <?php
                                foreach ($city as $c){
                                    ?>
                                    <tr>
                                        <td><a href="" style="color: #fff;text-decoration: none;"><?= $c->city ?></a></td>
                                        <td class="text-right"><?= \app\models\Control::getCountBalance($c->id); ?></td>
                                    </tr>
                                <?php
                                }
                                ?>


                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <h2 class="title-1 m-b-25">Баланс</h2>
                <div class="au-card au-card--bg-blue au-card-top-countries m-b-40">
                    <div class="au-card-inner">
                        <div class="table-responsive">
                            <table class="table table-top-countries">
                                <tbody>
                                <?php
                                foreach ($city as $c){
                                    ?>
                                    <tr>
                                        <td><?= $c->city ?></td>
                                        <td class="text-right"><?= \app\models\Control::getCountBalance($c->id,1); ?></td>
                                    </tr>
                                    <?php
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
