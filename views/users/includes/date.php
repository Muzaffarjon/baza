<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\Users */

$this->title = 'Поиска по дате';
$this->params['breadcrumbs'][] = ['label'=>$this->title,'template'=>"<li class='list-inline-item'>{link}</li>"];
?>
<?php Pjax::begin(['enablePushState'=>true,'timeout'=>10000,'class'=>'']); ?>
<div id="reload" class="row">
    <div class="col-md-2">
        <div class="card">
            <div class="card-header">
                <strong>Block Level Buttons </strong>
            </div>
            <div class="card-body" style="color: #fff;">
                <a type="button" href="<?= \yii\helpers\Url::to(['users/reports','type'=>'present']) ?>" class="btn btn-primary btn-lg btn-block">Презентаторы</a>
                <a type="button" href="<?= \yii\helpers\Url::to(['users/reports','type'=>'date']) ?>" class="btn btn-success btn-lg btn-block">Поиск по дате</a>

                <a type="button" href="<?= \yii\helpers\Url::to(['users/reports','type'=>'dose']) ?>" class="btn btn-primary btn-lg btn-block">Досье</a>
                <a type="button" href="<?= \yii\helpers\Url::to(['users/reports','type'=>'region']) ?>" class="btn btn-primary btn-lg btn-block">Регионы</a>
<!--                <a type="button" href="--><?//= \yii\helpers\Url::to(['users/reports','type'=>'investor']) ?><!--" class="btn btn-primary btn-lg btn-block">Sp инвестора</a>-->
            </div>
        </div>
    </div>
    <div class="col">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <strong>Просмотр отчета по периодам баланса </strong>
            </div>
            <div class="card-body" >
                <?php $form = ActiveForm::begin(); ?>
                <div class="row">

                    <div class="col-lg-5">
                        <form method="GET" action="<?= \yii\helpers\Url::to(['users/report']) ?>">
                        <div class="form-group field-users-lname">
                            <label class="control-label" for="users-lname">С периода</label>
                         <input type="date" id="users-lname" class="form-control" name="date_start" maxlength="50">

                            <div class="help-block"></div>
                        </div>                </div>
                    <div class="col-lg-5">
                        <div class="form-group field-users-lname">
                            <label class="control-label" for="users-lname">до</label>
                            <input type="date" id="users-lname" class="form-control" name="date_end" maxlength="50">

                            <div class="help-block"></div>
                        </div>
                    </div>

                    <div class="col-lg-2">
                        <input type="submit" value="Искать" class="btn btn-primary btn-block" style="line-height: 3;margin-top: 20px;">
                    </div>
                    </form>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>

        <?php if(isset($balance)){ ?>
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <strong>Просмотр отчета по периодам баланса </strong>
                </div>
                <div class="card-body" >
                    <div class="table-responsive table--no-card m-b-30">
                        <table class="table table-borderless table-striped table-earning">
                            <thead>
                            <tr>
                                <th>Ф.И.О</th>
                                <th class="text-right">Телефон</th>
                                <th class="text-right">Договор</th>
                                <th class="text-right">Деньги</th>
                                <th class="text-right">Время</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $s=0;
                            //                echo '<pre>';
                            //                var_dump($baleals);
                            //                echo '</pre>';
                            foreach ($balance as $bal){
                                echo "<tr>";
                                echo "<td class='text-left'>".$bal->deals->idLevel->idUser->name." ".$bal->deals->idLevel->idUser->lname." ".$bal->deals->idLevel->idUser->oname."</td>";
                                echo "<td class='text-right'>".$bal->deals->idLevel->idUser->phone."</td>";
                                echo "<td class='text-right'>".$bal->deals->deal_id."</td>";
                                echo "<td class='text-right'>".$bal->money."</td>";
                                echo "<td class='text-right'>".date('d-M-Y',$bal->datecreate)."</td>";
                                $s=$s+$bal->money;
                                echo  '</td>';
                                echo '</tr>';
                            }

                            ?>
                            <tr>
                                <td style="background: black;color: white;font-size: 20px;font-weight: 500;">&nbsp;</td>
                                <td style="background: black;color: white;font-size: 20px;font-weight: 500;">&nbsp;</td>
                                <td class='text-right' style="background: black;color: white;font-size: 20px;font-weight: 500;">Итог</td>
                                <td class='text-right' style="background: black;color: white;font-size: 20px;font-weight: 500;"><?= $s ?></td>
                                <td style="background: black;color: white;font-size: 20px;font-weight: 500;">&nbsp;</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <?php } ?>
        <?php Pjax::end(); ?>
