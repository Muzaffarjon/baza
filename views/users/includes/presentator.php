<?php

use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\Users */

$this->title = 'Презентатор';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['reports', 'type' => "present"]];
$this->params['breadcrumbs'][] = ['label'=>'Изменить','template'=>"<li class='list-inline-item'>{link}</li>"];
?>
<?php Pjax::begin(['enablePushState'=>true,'timeout'=>10000,'class'=>'']); ?>
<div id="reload" class="row">
    <div class="col-md-2">
        <div class="card">
            <div class="card-header">
                <strong>Block Level Buttons </strong>
            </div>
            <div class="card-body" style="color: #fff;">
                <a type="button" href="<?= \yii\helpers\Url::to(['users/reports','type'=>'present']) ?>" class="btn btn-success btn-lg btn-block">Презентаторы</a>
                <a type="button" href="<?= \yii\helpers\Url::to(['users/reports','type'=>'date']) ?>" class="btn btn-primary btn-lg btn-block">Поиск по дате</a>

                <a type="button" href="<?= \yii\helpers\Url::to(['users/reports','type'=>'dose']) ?>" class="btn btn-primary btn-lg btn-block">Досье</a>
                <a type="button" href="<?= \yii\helpers\Url::to(['users/reports','type'=>'region']) ?>" class="btn btn-primary btn-lg btn-block">Регионы</a>
<!--                <a type="button" href="--><?//= \yii\helpers\Url::to(['users/reports','type'=>'investor']) ?><!--" class="btn btn-success btn-lg btn-block">Sp инвестора</a>-->
            </div>
        </div>
    </div>
    <div class="col-md-9">
        <div class="card">
            <div class="card-header">
                <strong>Block Level Buttons </strong>
            </div>
            <div class="card-body" >
                <?php if(isset($level)){ ?>
                <div class="table-responsive table--no-card m-b-30">
                    <table class="table table-borderless table-striped table-earning">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Ф.И.О</th>
                            <th class="text-right">Телефон</th>
                            <th class="text-right">Sp Консултантов</th>
                            <th class="text-right">Уровень</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        //                echo '<pre>';
                        //                var_dump($l->idUsereals);
                        //                echo '</pre>';
                        $i=1;
                        foreach ($level as $l){
                            echo "<tr>";
                            echo "<td>".$i++."</td>";
                            echo "<td class='text-left'><a href='".\yii\helpers\Url::to(['users/deals','id'=>$l->idUser->id])."' >".$l->idUser->name." ".$l->idUser->lname." ".$l->idUser->oname."</a></td>";
                            echo "<td class='text-right'>".$l->idUser->phone."</td>";
                            echo "<td class='text-right'>".$l->idUser->level[0]->sp_unique."</td>";
                            echo "<td class='text-right'>".$l->level."</td>";
                            echo  '</td>';
                            echo '</tr>';
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            <?php }?>
            </div>
        </div>
    </div>
</div>
<?php Pjax::end(); ?>