<?php

use app\models\Users;
use yii\helpers\Html;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model app\models\Users */

$this->title = 'Оплата';
$this->params['breadcrumbs'][] = ['label'=>$this->title,'template'=>"<li class='list-inline-item'>{link}</li>"];

?>
<div class="row">
    <div class="col-lg-5 m-l-20">
        <div class="card">
        <div class="card-body">
            <?php $form = ActiveForm::begin(); ?>
            <?= $form->errorSummary($model); ?>
            <?= $form->field($model, 'money')->textInput() ?>

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-lg btn-info btn-block' : 'btn btn-lg btn-info btn-block']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
        </div>
    </div>

    <div class="col-lg-5">
        <div class="card">
            <div class="card-header">
                <strong>Инфо</strong>
                <small> Пользователя</small>
            </div>
            <div class="card-body card-block">
                <div class="form-group">
                    <label for="company" class=" form-control-label">Ф.И.О.</label>
                    <input type="text" id="company" value="<?= $user->idLevel->idUser->name.' '.$user->idLevel->idUser->lname.' '.$user->idLevel->idUser->oname; ?>" disabled="disabled" class="form-control">
                </div>
                <div class="form-group">
                    <label for="vat" class=" form-control-label">Sp инвестора</label>
                    <input type="text" id="vat" disabled="disabled" value="<?= $user->idLevel->sp_unique ?>" class="form-control">
                </div>
                <div class="form-group">
                    <label for="street" class=" form-control-label">Адрес</label>
                    <input type="text" id="street"  disabled="disabled" value="<?= $user->idLevel->idUser->address; ?>" class="form-control">
                </div>
                <div class="form-group">
                    <label for="street" class=" form-control-label">ИНН</label>
                    <input type="text" id="street"  disabled="disabled" value="<?= $user->idLevel->idUser->inn; ?>" class="form-control">
                </div>
                <div class="form-group">
                    <label for="city" class=" form-control-label">Город</label>
                    <input type="text" disabled="disabled" id="city" value="<?= $user->idLevel->idUser->idCity->city ?>" class="form-control">
                </div>
                <div class="form-group">
                    <label for="postal-code" class=" form-control-label">Номер телефон</label>
                    <input type="text" disabled="disabled"  id="postal-code" value="<?= $user->idLevel->idUser->phone ?>" class="form-control">
                </div>

            </div>
        </div>
    </div>
</div>