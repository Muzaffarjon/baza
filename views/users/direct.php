<?php

use app\models\Control;
use yii\helpers\Html;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model app\models\Users */

$this->title = 'Похожий пользователь';
$this->params['breadcrumbs'][] = ['label'=>$this->title,'template'=>"<li class='list-inline-item'>{link}</li>"];
?>
<div class="row">
    <div class="col-md-4">
        <div class="card">
            <div class="card-header">
                <strong class="card-title"><?= Html::encode($this->title) ?></strong>
            </div>
            <div
                    class="card-body">
                <?php $form = ActiveForm::begin(); ?>
                <?= $form->errorSummary($level); ?>
                <?php
                        echo '<div class="row">';
                        echo '<div class="col-lg-6">';
                        echo $form->field($level, 'sp_int')->textInput(['readonly'=>true,'value'=>Control::getSpUnique()]);
                        echo '</div>';
                        echo '<div class="col-lg-6">';
                        echo $form->field($level, 'sp_unique')->textInput(['maxlength' => true]);

                        echo '</div>';
                        echo '</div>';
                        echo  Html::submitButton('Пригласить', ['class' => 'btn btn-lg btn-primary btn-block']) ;
                ?>

                <?php $form = ActiveForm::end(); ?>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="sufee-alert alert with-close alert-danger alert-dismissible fade show">
            <span class="badge badge-pill badge-danger">Удачный</span>
            Напоминаем, что у нас в базе данных существует такой пользователь. Для приглашения нажмите " пригласить" еще раз
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
        <div class="card">
            <div class="card-header">
                <strong class="card-title"><?= Html::encode($this->title) ?></strong>
            </div>
            <?= $this->render('_formD', [
                'model' => $model,
            ]) ?>
        </div>
    </div>
</div>