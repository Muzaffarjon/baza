<?php

use app\models\Users;
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Users */

$this->title = 'Платежи';
$this->params['breadcrumbs'][] = ['label'=>$this->title,'template'=>"<li class='list-inline-item'>{link}</li>"];
//$deals=\app\models\Deals::find()->where(['status'=>1])->orderBy(['datecreate'=>SORT_DESC])->all();


?>
<div class="row">
    <div class="col-lg-12">
        <div class="table-responsive table--no-card m-b-30">
            <table class="table table-borderless table-striped table-earning">
                <thead>
                <tr>
                    <th>Ф.И.О</th>
                    <th class="text-right">SP Консультант</th>
                    <th class="text-right">SP Консультант</th>
                    <th class="text-right">Уровень</th>
<!--                    <th class="text-right">Инн</th>-->
                    <th class="text-right">Деньги</th>
<!--                    <th class="text-right">Время регистрации</th>-->
                    <th class="text-right">Операция</th>
                </tr>
                </thead>
                <tbody>
                <?php
//                echo '<pre>';
//                var_dump($deals);
//                echo '</pre>';
                foreach ($deals as $d){
                 $balance=\app\models\Control::checkForBalance($d->id);
                 $invited=\app\models\Control::getInvitedLevel($d->deals->idLevel->sp_int);

                    echo "<tr>";
                    echo "<td>".$d->deals->idLevel->idUser->name." ".$d->deals->idLevel->idUser->lname." ".$d->deals->idLevel->idUser->oname."</td>";
                    echo "<td class='text-right'><a href='".\yii\helpers\Url::to(['users/view','id'=>$invited])."'>".$d->deals->idLevel->sp_int."</a></td>";
                    echo "<td class='text-right'>".$d->deals->idLevel->sp_unique."</td>";
                    echo "<td class='text-right'>".$d->deals->idLevel->level."</td>";
                    //echo "<td class='text-right'>".$d->deals->idLevel->idUser->inn."</td>";
                    echo "<td class='text-right'>".$d->money."</td>";
                   // echo "<td class='text-right'>".date('d-M-Y',$d->datecreate)."</td>";

                    echo "<td class='text-right'>";
                    echo '<a href="'.\yii\helpers\Url::to(['users/cvitance','id'=>$d->deals_id]).'" class="btn btn-warning btn-sm m-r-10">
                      <i class="fa fa-list-ol"></i> Квитанция
                      </a>';
                    echo '<a href="'.\yii\helpers\Url::to(['users/unpay','id'=>$d->deals_id]).'" class="btn btn-danger btn-sm">
                      <i class="fa fa-trash"></i> Отмена оплаты
                      </a>';
                    echo "</td>";

                    echo '</tr>';
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>

</div>