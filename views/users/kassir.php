<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Users */

$this->title = 'Баланс';
$this->params['breadcrumbs'][] = ['label'=>$this->title,'template'=>"<li class='list-inline-item'>{link}</li>"];
?>
<div class="row">
    <div class="col-lg-6">
        <div class="table-responsive table--no-card m-b-30">
            <table class="table table-borderless table-striped table-earning">
                <thead>
                <tr>
                    <th>Ф.И.О</th>
                    <th>Баланс</th>
                </tr>
                </thead>
                <tbody>
                <?php
                //                echo '<pre>';
                //                var_dump($deals);
                //                echo '</pre>';
                foreach ($kassir as $d){
                    $balance=\app\models\Control::checkBalance($d->id);


                    echo "<tr>";
                    echo "<td>".$d->name." ".$d->lname." ".$d->oname."</td>";
                    echo "<td>".$balance."</td>";

                    echo '</tr>';
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>

</div>