<?php

use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\Users */

$this->title = 'Баланс';
$this->params['breadcrumbs'][] = ['label'=>$this->title,'template'=>"<li class='list-inline-item'>{link}</li>"];
?>
<?php Pjax::begin(['enablePushState'=>true,'timeout'=>10000,'class'=>'']); ?>
<div id="reload" class="row">

    <div class="col-md-2">
        <div class="card">
            <div class="card-header">
                <strong>Block Level Buttons </strong>
            </div>
            <div class="card-body" style="color: #fff;">
                <a type="button" href="<?= \yii\helpers\Url::to(['users/reports','type'=>'present']) ?>" class="btn btn-success btn-lg btn-block">Презентаторы</a>
                <a type="button" href="<?= \yii\helpers\Url::to(['users/reports','type'=>'date']) ?>" class="btn btn-primary btn-lg btn-block">Поиск по дате</a>

                <a type="button" href="<?= \yii\helpers\Url::to(['users/reports','type'=>'dose']) ?>" class="btn btn-primary btn-lg btn-block">Досье</a>
                <a type="button" href="<?= \yii\helpers\Url::to(['users/reports','type'=>'region']) ?>" class="btn btn-primary btn-lg btn-block">Регионы</a>
                <a type="button" href="<?= \yii\helpers\Url::to(['users/reports','type'=>'investor']) ?>" class="btn btn-primary btn-lg btn-block">Sp инвестора</a>
            </div>
        </div>
    </div>

    <div class="col-md-8">

        <div class="card">
            <div class="card-header">
                <strong>Block Level Buttons </strong>
                <small>Use this class
                    <code>.btn-block</code>
                </small>
            </div>
            <div class="card-body" >
                <button type="button" class="btn btn-outline-primary btn-lg btn-block">Block level button</button>
                <button type="button" class="btn btn-outline-secondary btn-lg btn-block">Block level button</button>

                <button type="button" class="btn btn-outline-primary btn-lg btn-block">Primary</button>
                <button type="button" class="btn btn-outline-secondary btn-lg btn-block">Secondary</button>
                <button type="button" class="btn btn-outline-success btn-lg btn-block">Success</button>
                <button type="button" class="btn btn-outline-warning btn-lg btn-block">Warning</button>
                <button type="button" class="btn btn-outline-danger btn-lg btn-block">Danger</button>
                <button type="button" class="btn btn-outline-link btn-lg btn-block">Link</button>
            </div>
        </div>

    </div>

</div>
<?php Pjax::end(); ?>