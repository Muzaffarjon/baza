<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Users */

$this->title = 'Должники';
$this->params['breadcrumbs'][] = ['label'=>$this->title,'template'=>"<li class='list-inline-item'>{link}</li>"];
$deals=\app\models\Deals::find()->where(['status'=>1])->orderBy(['datecreate'=>SORT_DESC])->all();
?>
<div class="row">
    <div class="col-lg-12">
        <div class="table-responsive table--no-card m-b-30">
            <table class="table table-borderless table-striped table-earning">
                <thead>
                <tr>
                    <th>Ф.И.О</th>
                    <th class="text-right">SP Консультанта</th>
                    <th class="text-right">SP Инвестора</th>
                    <th class="text-right">ID Договор</th>
                    <th class="text-right">Уровень</th>
                    <th class="text-right">Телефон</th>
                    <th class="text-right">Время регистрации</th>
                </tr>
                </thead>
                <tbody>
                <?php
                //                echo '<pre>';
                //                var_dump($deals);
                //                echo '</pre>';
                foreach ($deals as $d){
                    $balance=\app\models\Control::checkForBalance($d->id);

                    if($balance==0){
                    echo "<tr>";
                    echo "<td>".$d->idLevel->idUser->name." ".$d->idLevel->idUser->lname." ".$d->idLevel->idUser->oname."</td>";
                    echo "<td class='text-right'>".$d->idLevel->sp_int."</td>";
                    echo "<td class='text-right'>".$d->idLevel->sp_unique."</td>";
                    echo "<td class='text-right'>".$d->deal_id."</td>";
                    echo "<td class='text-right'>".$d->idLevel->level."</td>";
                    echo "<td class='text-right'>".$d->idLevel->idUser->phone."</td>";
                    echo "<td class='text-right'>".date('d-M-Y',$d->datecreate)."</td>";
                    echo '</tr>';
                }
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>

</div>