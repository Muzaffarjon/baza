<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Control;


/* @var $this yii\web\View */
/* @var $model app\models\Users */

$this->title = 'Обновить участника';
$this->params['breadcrumbs'][] = ['label'=>$this->title,'template'=>"<li class='list-inline-item'>{link}</li>"];
?>
<div class="row">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">
                <strong class="card-title"><?= Html::encode($this->title) ?></strong>
            </div>

            <?= $this->render('_formA', [
                'model' => $model,
                'type' => $type
            ]) ?>

        </div>
    </div>

    <div class="col-md-4">
        <div class="card">
            <div class="card-header">
                <strong class="card-title"><?= Html::encode($this->title) ?></strong>
            </div>
            <div
                class="card-body">
            <?php $form = ActiveForm::begin(); ?>

            <?php
            foreach ($model->level as $level){
                $deal=Control::checkDeals($level->id);
                if(!empty($deal)){
            echo '<div class="row">';
                echo '<div class="col-lg-6">';
                echo '<input type="hidden" name="Level[id]" value="'.$deal.'">';
                echo $form->field($level, 'sp_int')->textInput(['maxlength' => true,'readonly'=>true]);
                echo '</div>';
                echo '<div class="col-lg-6">';
                echo $form->field($level, 'sp_unique')->textInput(['maxlength' => true,'readonly'=>true]);

            echo '</div>';
            echo '</div>';
            }else{
                    echo '<div class="row">';
                    echo '<div class="col-lg-6">';
                    echo '<input type="hidden" name="Level[id]" value="'.$deal.'">';
                    echo $form->field($level, 'sp_int')->textInput(['maxlength' => true]);
                    echo '</div>';
                    echo '<div class="col-lg-6">';
                    echo $form->field($level, 'sp_unique')->textInput(['maxlength' => true]);

                    echo '</div>';
                    echo '</div>';
                    echo  Html::submitButton('Изменить', ['class' => 'btn btn-lg btn-primary btn-block']) ;
                }
            }
            ?>

            <?php $form = ActiveForm::end(); ?>
            </div>
        </div>
    </div>

</div>
