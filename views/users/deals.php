<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model app\models\Users */

$this->title = 'Печатать договор';
$this->params['breadcrumbs'][] = ['label'=>$this->title,'template'=>"<li class='list-inline-item'>{link}</li>"];
?>

<div class="row">
    <div class="col-lg-6">
        <div class="user-data m-b-30">
            <h3 class="title-3 m-b-30">
                <i class="zmdi zmdi-account-calendar"></i>Договоры пользователья</h3>
            <div class="table-responsive table-data">
                <table class="table">
                    <thead>
                    <tr>
                        <td>Id договор</td>
                        <td>Уровень</td>
                        <td>Время распечатки</td>
                        <td>Sp Консультанта</td>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach ($user->level as $level){
                        //var_dump($level->id);
                        foreach ($level->deals as $deal){
                            ?>
                            <tr>

                                <td>
                                    <div class="table-data__info">
                                        <h6><?= $deal->id ?></h6>
                                        <span>
                                                                <a href="#"><?= $deal->deal_id ?></a>
                                                            </span>
                                    </div>
                                </td>
                                <td>
                                    <span ><?= $deal->idLevel->level; ?></span>
                                </td>
                                <td>
                                    <?= date('d.m.Y H:i:s',$deal->datecreate) ?>
                                </td>
                                <td><?= $deal->idLevel->sp_int; ?></td>
                            </tr>
                        <?php }} ?>
                    </tbody>
                </table>
            </div>
            <div class="user-data__footer">
            </div>
        </div>
    </div>
    <div class="col-lg-5">
        <div class="card">
            <div class="card-header">
                <strong>Инфо</strong>
                <small> Пользователя</small>
            </div>
            <div class="card-body card-block">
                <div class="form-group">
                    <label for="company" class=" form-control-label">Ф.И.О.</label>
                    <input type="text" id="company" value="<?= $user->name.' '.$user->lname.' '.$user->oname; ?>" disabled="disabled" class="form-control">
                </div>
                <div class="form-group">
                    <label for="vat" class=" form-control-label">Email</label>
                    <input type="text" id="vat" disabled="disabled" value="<?= $user->email ?>" class="form-control">
                </div>
                <div class="form-group">
                    <label for="street" class=" form-control-label">Адрес</label>
                    <input type="text" id="street"  disabled="disabled" value="<?= $user->address; ?>" class="form-control">
                </div>
                <div class="form-group">
                    <label for="street" class=" form-control-label">ИНН</label>
                    <input type="text" id="street"  disabled="disabled" value="<?= $user->inn; ?>" class="form-control">
                </div>
                <div class="form-group">
                    <label for="city" class=" form-control-label">Город</label>
                    <input type="text" disabled="disabled" id="city" value="<?= $user->idCity->city ?>" class="form-control">
                </div>
                <div class="form-group">
                    <label for="postal-code" class=" form-control-label">Номер телефон</label>
                    <input type="text" disabled="disabled"  id="postal-code" value="<?= $user->phone ?>" class="form-control">
                </div>

            </div>
        </div>
    </div>
</div>