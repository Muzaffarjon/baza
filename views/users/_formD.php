<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Users */
/* @var $form yii\widgets\ActiveForm */
?>
<style type="text/css">
    /*$bg: #332f35;*/
    /*$fg: lighten($bg,20%);*/
    /*$borderWidth: 3px;*/

    input[type=radio] {
        position: absolute;
        visibility: hidden;
        display: none;
    }

    label.radio {
        color: #332f35;
        display: inline-block;
        cursor: pointer;
        font-weight: bold;
        padding: 5px 45px;
        margin: 0;
    }

    input[type=radio]:checked + label.radio{
        color: #fff;
        background: #28a745;
    }

    .radio-group {
        border: solid 3px #28a745;
        display: inline-block;
        margin: 35px 0 0 0;
        border-radius: 10px;
        overflow: hidden;
    }

    @import url('https://fonts.googleapis.com/css?family=Roboto');
</style>
<script src="<?= Yii::$app->request->baseUrl;  ?>/master/js/jquery.maskedinput.min.js"></script>
<script defer src="https://unpkg.com/imask"></script>
<script src="https://unpkg.com/jquery-input-mask-phone-number@1.0.11/dist/jquery-input-mask-phone-number.js"></script>
<script>
    document.addEventListener("DOMContentLoaded", function () {
    var dateMask = IMask(
        document.getElementById('date-mask'),
        {
            mask: Date,
            min: new Date(1940, 0, 1),
            max: new Date(2050, 0, 1),
            lazy: false
        }
    );
    var dateMask = IMask(
        document.getElementById('date-mask1'),
        {
            mask: Date,
            min: new Date(1940, 0, 1),
            max: new Date(2050, 0, 1),
            lazy: false
        }
    );

        $("#dynamic-mask").mask("(99) 999-99-99");
        //$("#pinn").mask("9999999");
    });

    $(document).ready(function () {
        $('#dynamic-mask').usPhoneFormat({
            format: '+992(xx)xxx-xx-xx',
        });
    });
</script>
<?php
$city=\app\models\City::find()->all();
$arr=[];
foreach ($city as $c)
    $arr[$c->id]=$c->city;
//var_dump($arr);
    ?>

        <div class="card-body">
            <?php $form = ActiveForm::begin(); ?>

            <?= $form->errorSummary($model); ?>
            <?php if(Yii::$app->user->identity->role!=1){ ?>
                <?= $form->field($model, 'login')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>
            <?php } ?>
            <div class="row">
                <div class="col-lg-6">
                    <?= $form->field($model, 'lname')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-lg-6">
                    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-lg-6">
                    <?= $form->field($model, 'oname')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-lg-6">
                    <?= $form->field($model, 'email')->textInput() ?>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-6">
                    <?= $form->field($model, 'date_birth')->textInput(['id'=>'date-mask','maxlength' => true, 'value' => Yii::$app->formatter->asDatetime($model->date_birth, 'php:d.m.Y')]) ?>
                </div>
                <div class="col-lg-6 dasd">
                    <div class="radio-group">
                        <?php
                        if($model->isNewRecord){
                            ?>
                            <input name="Users[sex]"  checked type="radio" value="0" id="option-one" name="selector" >
                            <label class="radio" for="option-one">Женщина</label>
                            <input name="Users[sex]" type="radio" value="1" id="option-two" name="selector">
                            <label class="radio" for="option-two">Мужчина</label>
                        <?php }else{ ?>
                            <?php
                            if($model->sex==0){
                                ?>
                                <input name="Users[sex]"  checked type="radio" value="0" id="option-one" name="selector" >
                                <label class="radio" for="option-one">Женщина</label>
                                <input name="Users[sex]" type="radio" value="1" id="option-two" name="selector">
                                <label class="radio" for="option-two">Мужчина</label>
                                <?php
                            }else{
                                ?>
                                <input name="Users[sex]"   type="radio" value="0" id="option-one" name="selector" >
                                <label class="radio" for="option-one">Женщина</label>
                                <input name="Users[sex]" checked type="radio" value="1" id="option-two" name="selector">
                                <label class="radio" for="option-two">Мужчина</label>
                            <?php } ?>

                        <?php } ?>
                    </div>
                    <?//= $form->field($model, 'sex')->radioList( [0=>'zero', 1 => 'one', 2 => 'two'] )->label(false) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <?= $form->field($model, 'passport_number')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-lg-6">
                    <?= $form->field($model, 'where_passport')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6"><?= $form->field($model, 'date_passport')->textInput(['id'=>'date-mask1','maxlength' => true, 'value' => Yii::$app->formatter->asDatetime($model->date_passport, 'php:d.m.Y')]) ?></div>
                <div class="col-lg-6"><?= $form->field($model, 'inn')->textInput(['id'=>'pinn']) ?></div>
            </div>
            <div class="row">
                <div class="col-lg-6"><?= $form->field($model, 'phone')->textInput(['id'=>'dynamic-mask','placeholder'=>'Пример: (92) 834-40-74']) ?></div>
                <div class="col-lg-6"><?= $form->field($model, 'id_city')->dropDownList($arr) ?></div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>

