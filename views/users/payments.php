<?php

use app\models\Users;
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Users */

$this->title = 'Платежи';
$this->params['breadcrumbs'][] = ['label'=>$this->title,'template'=>"<li class='list-inline-item'>{link}</li>"];
$deal=Users::find()->where(['role'=>1])->all();


?>
<div class="row">
    <div class="col-lg-12">
        <?php
        foreach ($deal as $pre){
            $deals=\app\models\Control::getAllDeptors($pre->level[0]->sp_unique);
            if($deals!=null){
            echo '
                                <div class="card">
                                    <div class="card-header bg-success">
                                        <a class="card-title text-light"><a href="'.\yii\helpers\Url::to(['users/view','id'=>$pre->id]).'" style="color: #fff; text-decoration: none;font-weight: 700">'.$pre->lname.' '.$pre->name.' '.$pre->oname.'</a></strong>
                                    </div>
                                    ';

            ?>

            <table class="table table-borderless table-striped table-earning">
                <thead>
                <tr>
                    <th>Ф.И.О</th>
                    <th class="text-right">Уровень</th>
                    <th class="text-right">Инн</th>
                    <th class="text-right">Договор</th>
                    <th class="text-right">Деньги</th>
                    <th class="text-right">Время регистрации</th>
                    <th class="text-right">Операции</th>
                </tr>
                </thead>
                <tbody>
                <?php
               // echo '<pre>';
                //                var_dump($deals);
                //                echo '</pre>';
                foreach ($deals as $d){
                    $balance=\app\models\Control::checkForBalance($d->id);


                    echo "<tr>";
                    echo "<td>".$d->idLevel->idUser->name." ".$d->idLevel->idUser->lname." ".$d->idLevel->idUser->oname."</td>";
                    echo "<td class='text-right'>".$d->idLevel->level."</td>";
                    echo "<td class='text-right'>".$d->idLevel->idUser->inn."</td>";
                    echo "<td class='text-right'>".$d->deal_id."</td>";
                    echo "<td class='text-right'>".\app\models\Control::getMoneyFor($d->idLevel->level)."</td>";

                    echo "<td class='text-right'>".date('d-M-Y',$d->datecreate)."</td>";
                    echo '<td class="text-right">';
                    if($balance!=1) echo '<a href="'.\yii\helpers\Url::to(['users/pay','id'=>$d->id]).'" class="btn btn-primary btn-sm">
                                             Оплатить
                                        </a>';
                    else{
                        echo '<a href="'.\yii\helpers\Url::to(['users/cvitance','id'=>$d->id]).'" class="btn btn-warning btn-sm m-r-10">
                      <i class="fa fa-list-ol"></i> Квитанция
                      </a>';
                        echo '<a href="'.\yii\helpers\Url::to(['users/unpay','id'=>$d->id]).'" class="btn btn-danger btn-sm">
                      <i class="fa fa-trash"></i> Отмена оплаты
                      </a>';
                    }
                    echo  '</td>';
                    echo '</tr>';
                }
                ?>
                </tbody>
            </table>

            <?php
                         echo        '</div>
                                
                            ';
        }
        }
        ?>
        <div class="table-responsive table--no-card m-b-30">



        </div>
    </div>

</div>