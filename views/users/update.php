<?php

use app\models\Control;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Users */

$this->title = 'Изменить пользователя: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['invited']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = ['label'=>'Изменить','template'=>"<li class='list-inline-item'>{link}</li>"];
?>
<div class="row">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">
                <strong class="card-title"><?= Html::encode($this->title) ?></strong>
            </div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

        </div>
    </div>
    <div class="col-md-4">
        <div class="card">
            <div class="card-header">
                <strong class="card-title"><?= Html::encode($this->title) ?></strong>
            </div>
            <div
                    class="card-body">
                <?php  ?>

                <?php
                foreach ($model->level as $level){
                    $form = ActiveForm::begin(['options'=>['style'=>'margin-bottom:25px;']]);
                    $deal=Control::checkDeals($level->id);
                    if(!empty($deal)){
                        echo '<div class="row">';
                            echo '<div class="col-lg-6">';
                        echo '<input type="hidden" name="Level[id]" value="'.$level->id.'">';
                        echo $form->field($level, 'sp_int')->textInput(['maxlength' => true,'readonly'=>true]);
                        echo '</div>';
                        echo '<div class="col-lg-6">';
                        echo $form->field($level, 'sp_unique')->textInput(['maxlength' => true,'readonly'=>true]);

                        echo '</div>';
                        echo '</div>';
                    }else{
                        if($level->sp_int==Control::getSpUnique()){
                        echo '<div class="row">';
                        echo '<div class="col-lg-6">';
                        echo '<input type="hidden" name="Level[id]" value="'.$level->id.'">';
                        echo $form->field($level, 'sp_int')->textInput(['maxlength' => true,'readonly'=>true]);
                        echo '</div>';
                        echo '<div class="col-lg-6">';
                        echo $form->field($level, 'sp_unique')->textInput(['maxlength' => true]);

                        echo '</div>';
                        echo '</div>';
                        echo  Html::submitButton('Изменить', ['class' => 'btn btn-lg btn-primary btn-block']) ;
                    }
                    }
                    $form = ActiveForm::end();
                    echo '<hr>';
                }
                ?>

                <?php  ?>
            </div>
        </div>
    </div>
</div>
