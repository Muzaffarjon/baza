<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = ['label'=>$this->title,'template'=>"<li class='list-inline-item'>{link}</li>"];
?>
<style>
    .table a{
        font-size: 19px;
        padding-right:3px;
    }
</style>
<script type="text/javascript">

    function update(id) {
        var data = $('#check_'+id).val();
        $.ajax({
            type: 'POST',
            url: "<?php echo Url::to(['users/status']); ?>",
            cache: false,
            data: {id:id,value:data},
            error: function () {

            },
            beforeSend: function () {
                $('checkbox').attr('disabled', 'disabled');
            },
            success: function (data) {
                $('checkbox').removeAttr('disabled');
                //location.reload();
                $.pjax.reload({container: '#pjax-container'});
            }
        })
    }

</script>
<div class="row">
    <div class="col-md-12 ">
        <div class="row"><div class="col-md-6"><div class="card-body">
                    <a class="btn <?= $_GET['type']==0 ? 'btn-success' : 'btn-primary' ?>" href="<?= Url::to(['users/index','type'=>0]) ?>">Инвесторы</a>
                    <a class="btn <?= $_GET['type']==1  ? 'btn-success' : 'btn-primary' ?>" href="<?= Url::to(['users/index','type'=>1]) ?>">Презентаторы</a>
                    <a class="btn <?= $_GET['type']==2 ? 'btn-success' : 'btn-primary' ?>" href="<?= Url::to(['users/index','type'=>2]) ?>">Кассиры</a>
                    <a class="btn <?= $_GET['type']==3 ? 'btn-success' : 'btn-primary' ?>" href="<?= Url::to(['users/index','type'=>3]) ?>">Админы</a></div></div>
        <div class="col-md-6">
            <?php if(Yii::$app->user->identity->role==3 or Yii::$app->user->identity->role==4) echo '<div class="card-body">
                                        <a href="'.Url::to(["users/add","type"=>1]).'" class="btn btn-primary">Добавить Презентатора</a> <a href="'.Url::to(["users/add","type"=>2]).'" class="btn btn-secondary">Добавить Кассира</a>';
            if(Yii::$app->user->identity->role==4) echo '<a href="'.Url::to(["users/add","type"=>3]).'" class="btn btn-success">Добавить Администратора</a>' ;
            echo  '</div>';?>
        </div></div>
        <?php Pjax::begin(['id' => 'pjax-container']); ?>
        <div class="table-responsive table-responsive table-responsive-data3" style="margin-bottom: 25px;">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'tableOptions' => [
                    'class' => 'table table-borderless table-data3'
                ],
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    //'id',
                    //'login',
                    //'genauto',
                    [
                        'attribute' => 'fio',
                        'format' => 'raw',
                        'value' => function ($model) {
                            return $model->name.' '.$model->lname.' '.$model->oname;
                        },
                    ],
                    'login',
                    'genauto',
                    [
                        'attribute'=>'date_birth',
                        'value'=>function($model){
                            return \app\models\Control::getDate($model->date_birth);
                        },
                        'format'=>'raw'
                    ],
                    'inn',
                    [
                        'attribute' => 'status',
                        'format' => 'raw',
                        'value' => function ($model) {
                            if(Yii::$app->user->identity->role!=1)


                                if ($model->status === 0) {
                                    return '<label id="form_'.$model->id.'" class="switch">
                                        <input id="check_'.$model->id.'" onclick="update('.$model->id.')" value=1 type="checkbox">
                                          <span class="slider round"></span>
                                        </label>'; // "x" icon in red color
                                }elseif($model->status==1){
                                    return '<label id="form_'.$model->id.'"  class="switch">
                                        <input id="check_'.$model->id.'" onclick="update('.$model->id.')" value=0 type="checkbox" checked>
                                          <span class="slider round"></span>
                                        </label>'; // check icon
                                }else{
                                    return '<label  class="switch">
                                        <input  value=0 type="checkbox" disabled>
                                          <span class="slider round"></span>
                                        </label>';
                                }
                        },
                    ],
                    [
                        'attribute' => 'role',
                        'format' => 'raw',
                        'value' => function ($model) {
                            switch ($model->role){
                                case 0:
                                    return '<h3><span class="badge badge-light">Инвестор</span></h3>';
                                case 1:
                                    return '<h3><span class="badge badge-secondary">Презентатор</span></h3>';
                                    break;
                                case 2:
                                    return '<h3><span class="badge badge-primary">Кассир</span></h3>';
                                    break;
                                case 3:
                                    return '<h3><span class="badge badge-warning">Админ</span></h3>';
                                    break;
                            }
                        },
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'header' => 'Actions',
                        'headerOptions' => ['style' => 'color:#337ab7'],
                        'template' => '{agree}{view}{update}',
                        'buttons' => [
                            'agree' => function ($url, $model) {
                                if($model->role==0)
                                return Html::a('<button type="button" class="btn btn-success m-l-10"><span class="fa fa-list-alt"></span></button>', $url, [
                                    'title' => Yii::t('app', 'Договор'),
                                ]);
                            },
                            'view' => function ($url, $model) {
                                if($model->status==1){
                                return Html::a('<button type="button" class="btn btn-info m-l-10"><span class="fa fa-eye"></span></button>', $url, [
                                    'title' => Yii::t('app', 'Просмотр'),
                                ]);
                                }elseif($model->status==2){
                                    return Html::a('<button type="button" class="btn btn-success m-l-10"><span class="fa fa-eye"></span></button>', $url, [
                                        'title' => Yii::t('app', 'Просмотр'),
                                    ]);
                                }else{
                                    return Html::a('<button type="button" class="btn btn-danger m-l-10"><span class="fa fa-eye"></span></button>', $url, [
                                        'title' => Yii::t('app', 'Просмотр'),
                                    ]);
                                }
                            },

                            'update' => function ($url, $model) {
                                $now = strtotime("-10 minutes");
                                if(Yii::$app->user->identity->role==1){
                                    if ($now > $model->datecreate) {

                                    }else
                                        return Html::a('<button type="button" class="btn btn-primary m-l-10"><span class="zmdi zmdi-edit"></span></button>', $url, ['class'=>'item' ,
                                            'title' => Yii::t('app', 'update'),
                                        ]);
                                }else{

                                return Html::a('<button type="button" class="btn btn-primary m-l-10"><span class="zmdi zmdi-edit"></span></button>', $url, ['class'=>'item' ,
                                    'title' => Yii::t('app', 'Изменить'),
                                ]);
                                }
                            },

                        ],
                        'urlCreator' => function ($action, $model, $key, $index) {
                            if ($action === 'agree') {
                                $url =Url::to(['users/deals','id'=>$model->id]);
                                return $url;
                            }
                            if ($action === 'view') {
                                $url =Url::to(['users/view','id'=>$model->id]);
                                return $url;
                            }

                            if ($action === 'update') {
                                $url =Url::to(['users/upd','id'=>$model->id,'type'=>$model->role]);
                                return $url;
                            }
                        }
                    ],
                ],
            ]); ?>
        </div>
        <?php Pjax::end(); ?>
    </div>
</div>
