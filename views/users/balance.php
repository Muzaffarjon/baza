<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Users */

$this->title = 'Баланс';
$this->params['breadcrumbs'][] = ['label'=>$this->title,'template'=>"<li class='list-inline-item'>{link}</li>"];
?>
<div class="row">
    <div class="row" style="margin-left: 25px;">
        <div class="col-md-6 col-lg-3">
            <div class="statistic__item">
                <h2 class="number"><?= \app\models\Control::getDealsByLevel(1); ?></h2>
                <span class="desc">1 Уровень Договоры</span>
                <div class="icon">
                    <i class="zmdi zmdi-account-o"></i>
                </div>
            </div>
        </div><div class="col-md-6 col-lg-3">
            <div class="statistic__item">
                <h2 class="number"><?= \app\models\Control::getDealsByLevel(2); ?></h2>
                <span class="desc">2 Уровень Договоры</span>
                <div class="icon">
                    <i class="zmdi zmdi-account-o"></i>
                </div>
            </div>
        </div><div class="col-md-6 col-lg-3">
            <div class="statistic__item">
                <h2 class="number"><?= \app\models\Control::getDealsByLevel(3); ?></h2>
                <span class="desc">3 Уровень Договоры</span>
                <div class="icon">
                    <i class="zmdi zmdi-account-o"></i>
                </div>
            </div>
        </div><div class="col-md-6 col-lg-3">
            <div class="statistic__item">
                <h2 class="number"><?= \app\models\Control::getDealsByLevel(4); ?></h2>
                <span class="desc">4 Уровень Договоры</span>
                <div class="icon">
                    <i class="zmdi zmdi-account-o"></i>
                </div>
            </div>
        </div><div class="col-md-6 col-lg-3">
            <div class="statistic__item">
                <h2 class="number"><?= \app\models\Control::getMoney(1); ?></h2>
                <span class="desc">Баланс 1 уровеня</span>
                <div class="icon">
                    <i class="zmdi zmdi-account-o"></i>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-3">
            <div class="statistic__item">
                <h2 class="number"><?= \app\models\Control::getMoney(2); ?></h2>
                <span class="desc">Баланс 2 уровеня</span>
                <div class="icon">
                    <i class="zmdi zmdi-money"></i>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-3">
            <div class="statistic__item">
                <h2 class="number"><?= \app\models\Control::getMoney(3); ?></h2>
                <span class="desc">Баланс 3 уровеня</span>
                <div class="icon">
                    <i class="zmdi zmdi-money"></i>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-3">
            <div class="statistic__item">
                <h2 class="number"><?= \app\models\Control::getMoney(4); ?></h2>
                <span class="desc">Баланс 4 уровеня</span>
                <div class="icon">
                    <i class="zmdi zmdi-money"></i>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6">

            <div class="table-responsive table--no-card m-b-30">
                <table class="table table-borderless table-striped table-earning">
                    <thead>
                    <tr>
                        <th>Ф.И.О</th>
                        <th class="text-right">Телефон</th>
                        <th class="text-right">Деньги</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    //                echo '<pre>';
                    //                var_dump($deals);
                    //                echo '</pre>';
                    foreach ($deals as $d){
                        echo "<tr>";
                        echo "<td>".$d->deals->idLevel->idUser->name." ".$d->deals->idLevel->idUser->lname." ".$d->deals->idLevel->idUser->oname."</td>";
                        echo "<td class='text-right'>".$d->deals->idLevel->idUser->phone."</td>";
                        echo "<td class='text-right'>".$d->money."</td>";

                        echo  '</td>';
                        echo '</tr>';
                    }
                    ?>
                    </tbody>
                </table>
            </div>
    </div>
    <div class="col-lg-6">

            <div class="table-responsive table--no-card m-b-30">
                <table class="table table-borderless table-striped table-earning">
                    <thead>
                    <tr>
                        <th>Ф.И.О</th>
                        <th class="text-right">Деньги</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    //                echo '<pre>';
                    //                var_dump($deals);
                    //                echo '</pre>';
                    foreach ($bal as $d){
                        echo "<tr>";
                        echo "<td>".$d->idKassir->name." ".$d->idKassir->lname." ".$d->idKassir->oname."</td>";
                        echo "<td class='text-right'>".$d->money."</td>";
                        echo  '</td>';
                        echo '</tr>';
                    }
                    ?>
                    </tbody>
                </table>
            </div>
    </div>




</div>