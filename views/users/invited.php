<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Пользователи';
$this->params['breadcrumbs'][] = ['label'=>$this->title,'template'=>"<li class='list-inline-item'>{link}</li>"];
?>
<style>
    .table a{
        font-size: 19px;
        padding-right:3px;
    }
</style>
<script type="text/javascript">

    function update(id) {
        var data = $('#check_'+id).val();
        $.ajax({
            type: 'POST',
            url: "<?php echo Url::to(['users/status']); ?>",
            cache: false,
            data: {id:id,value:data},
            error: function () {

            },
            beforeSend: function () {
                $('checkbox').attr('disabled', 'disabled');
            },
            success: function (data) {
                $('checkbox').removeAttr('disabled');
                //location.reload();
                $.pjax.reload({container: '#pjax-container'});
            }
        })
    }

</script>
<div class="row">
    <div class="col-md-12 ">
        <div class="">
            <?php //echo strtotime("-10 minutes");?>
            <?php if(Yii::$app->user->identity->role!=1) echo '<div class="card-body">
                                        <a href="'.Url::to(["users/add","type"=>1]).'" class="btn btn-primary">Добавить Презентатора</a>
                                        <a href="'.Url::to(["users/add","type"=>2]).'" class="btn btn-secondary">Добавить Кассира</a>
                                        <a href="'.Url::to(["users/add","type"=>3]).'" class="btn btn-success">Добавить Администратора</a>
                                    </div>' ?>
        </div>
        <?php Pjax::begin(['id' => 'pjax-container']); ?>
        <div class="table-responsive table-responsive table-responsive-data3">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'tableOptions' => [
                    'class' => 'table table-borderless table-data3'
                ],
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    //'id',
                    //'login',
                    //'genauto',
                    [
                        'attribute' => 'fio',
                        'format' => 'raw',
                        'value' => function ($model) {
                            return $model->idUser->lname.' '.$model->idUser->name.' '.$model->idUser->oname;
                        },
                    ],
                    [
                        'attribute' => 'date_birth',
                        'format' => 'raw',
                        'value' => function ($model) {
                            return Yii::$app->formatter->asDatetime($model->idUser->date_birth, 'php:d.m.Y');
                        },
                    ],
                    [
                        'attribute' => 'inn',
                        'format' => 'raw',
                        'value' => function ($model) {
                            return $model->idUser->inn;
                        },
                    ],
                    [
                        'attribute' => 'phone',
                        'format' => 'raw',
                        'value' => function ($model) {
                            return $model->idUser->phone;
                        },
                    ],
                    [
                        'attribute' => 'level',
                        'format' => 'raw',
                        'value' => function ($model) {
                            return $model->level;
                        },
                    ],

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'header' => 'Actions',
                        'headerOptions' => ['style' => 'color:#337ab7'],
                        'template' => '{agree}{view}{update}',
                        'buttons' => [
                            'agree' => function ($url, $model) {
                                $check=\app\models\Control::checkDeals($model->id);

                                if($model->idUser->role==0){
                                if($check==1)
                                    return Html::a('<button type="button" class="btn btn-success m-l-10"><span class="fa fa-list-alt"></span></button>',"#", [
                                    'title' => Yii::t('app', 'Договор'),
                                ]);
                                elseif($check==0)
                                    return Html::a('<button type="button" class="btn btn-danger m-l-10"><span class="fa fa-list-alt"></span></button>', $url, [
                                        'title' => Yii::t('app', 'Договор'),
                                    ]);
                                }
                            },
                            'view' => function ($url, $model) {
                                return Html::a('<button type="button" class="btn btn-info m-l-10"><span class="fa fa-eye"></span></button>', $url, [
                                    'title' => Yii::t('app', 'Просмотр'),
                                ]);
                            },

                            'update' => function ($url, $model) {
                                $now = strtotime("-10 minutes");
                                if(Yii::$app->user->identity->role==1){
                                    if ($now < $model->datecreate)
                                        return Html::a('<button type="button" class="btn btn-primary m-l-10"><span class="zmdi zmdi-edit"></span></button>', $url, ['class'=>'item' ,
                                            'title' => Yii::t('app', 'update'),
                                        ]);
                                }else{

                                return Html::a('<button type="button" class="btn btn-primary m-l-10"><span class="zmdi zmdi-edit"></span></button>', $url, ['class'=>'item' ,
                                    'title' => Yii::t('app', 'Изменить'),
                                ]);
                                }
                            },

                        ],
                        'urlCreator' => function ($action, $model, $key, $index) {
                            if ($action === 'agree') {
                                $url =Url::to(['users/agreement','id'=>$model->id]);
                                return $url;
                            }
                            if ($action === 'view') {
                                $url =Url::to(['users/view','id'=>$model->idUser->id]);
                                return $url;
                            }

                            if ($action === 'update') {
                                $url =Url::to(['users/update','id'=>$model->idUser->id]);
                                return $url;
                            }
                        }
                    ],
                ],
            ]); ?>
        </div>
        <?php Pjax::end(); ?>
    </div>
</div>
