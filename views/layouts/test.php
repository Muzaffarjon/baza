<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\widgets\Menu;

//AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="en" class="wide wow-animation">
<head>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <script src="<?= Yii::$app->request->baseUrl; ?>/cdn-cgi/apps/head/3ts2ksMwXvKRuG480KNifJ2_JNM.js"></script><link rel="icon" href="<?= Yii::$app->request->baseUrl; ?>/images/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Arvo:400,700%7COpen+Sans:300,300italic,400,400italic,700italic,800%7CUbuntu:500">
    <link rel="stylesheet" href="<?= Yii::$app->request->baseUrl; ?>/css/style.css">
    <!--[if lt IE 10]>
    <div style="background: #212121; padding: 10px 0; box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3); clear: both; text-align:center; position: relative; z-index:1;"><a href="httpqwertywindows.microsoft.com/en-US/internet-explorer/"><img src="images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    <script src="<?= Yii::$app->request->baseUrl; ?>/js/html5shiv.min.js"></script>
    <![endif]-->
    <?php $this->head() ?>
</head>
<body style="">
<?php $this->beginBody() ?>
<div class="page">
    <div class="page-loader page-loader-variant-1">
        <div><a href="" class="brand brand-md brand-inverse"><img src="<?= Yii::$app->request->baseUrl; ?>/images/logo-light-145x30.png" alt="" width="145" height="30"/></a>
            <div class="page-loader-body">
                <div id="spinningSquaresG">
                    <div id="spinningSquaresG_1" class="spinningSquaresG"></div>
                    <div id="spinningSquaresG_2" class="spinningSquaresG"></div>
                    <div id="spinningSquaresG_3" class="spinningSquaresG"></div>
                    <div id="spinningSquaresG_4" class="spinningSquaresG"></div>
                    <div id="spinningSquaresG_5" class="spinningSquaresG"></div>
                    <div id="spinningSquaresG_6" class="spinningSquaresG"></div>
                    <div id="spinningSquaresG_7" class="spinningSquaresG"></div>
                    <div id="spinningSquaresG_8" class="spinningSquaresG"></div>
                </div>
            </div>
        </div>
    </div>
    <header class="page-head">
        <div class="rd-navbar-wrap">
            <nav data-layout="rd-navbar-fixed" data-sm-layout="rd-navbar-fixed" data-md-device-layout="rd-navbar-fixed" data-lg-layout="rd-navbar-static" data-lg-device-layout="rd-navbar-static" data-stick-up-clone="false" data-md-stick-up-offset="53px" data-lg-stick-up-offset="53px" data-md-stick-up="true" data-lg-stick-up="true" class="rd-navbar rd-navbar-corporate-light">
                <div class="bg-ebony-clay context-dark">
                    <div class="rd-navbar-inner">
                        <div class="rd-navbar-aside-wrap">
                            <div class="rd-navbar-aside">
                                <div data-rd-navbar-toggle=".rd-navbar-aside" class="rd-navbar-aside-toggle"><span></span></div>
                                <div class="rd-navbar-aside-content">
                                    <ul class="rd-navbar-aside-group list-units">
                                        <li>
                                            <div class="unit unit-horizontal unit-spacing-xs unit-middle">
                                                <div class="unit-left"><span class="icon icon-xxs icon-primary fa-map-marker"></span></div>
                                                <div class="unit-body"><a href="#" class="link-secondary">267 Park Avenue New York, NY 90210</a></div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="unit unit-horizontal unit-spacing-xs unit-middle">
                                                <div class="unit-left"><span class="icon icon-xxs icon-primary fa-clock-o"></span></div>
                                                <div class="unit-body"><span datetime="2016-04-30" class="time text-white">Mon – Sat: 9:00am–18:00pm. Sunday CLOSED</span></div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="unit unit-horizontal unit-spacing-xs unit-middle">
                                                <div class="unit-left"><span class="icon icon-xxs icon-primary fa-phone"></span></div>
                                                <div class="unit-body"><a href="callto:#" class="link-secondary">+1 (409) 987–5874</a></div>
                                            </div>
                                        </li>
                                    </ul>
                                    <div class="rd-navbar-aside-group">
                                        <ul class="list-inline list-inline-reset">
                                            <li><a href="#" class="icon icon-round icon-pciked-bluewood icon-xxs-smallest fa fa-facebook"></a></li>
                                            <li><a href="#" class="icon icon-round icon-pciked-bluewood icon-xxs-smallest fa fa-twitter"></a></li>
                                            <li><a href="#" class="icon icon-round icon-pciked-bluewood icon-xxs-smallest fa fa-google-plus"></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="rd-navbar-inner">
                    <div class="rd-navbar-group">
                        <div class="rd-navbar-panel">
                            <button data-rd-navbar-toggle=".rd-navbar-nav-wrap" class="rd-navbar-toggle"><span></span></button><a href="<?= Yii::$app->homeUrl; ?>" class="rd-navbar-brand brand"><img src="<?= Yii::$app->request->baseUrl; ?>/images/logo-145x30.png" alt="" width="145" height="30"/></a>
                        </div>
                        <div class="rd-navbar-group-asside">
                            <div class="rd-navbar-nav-wrap">
                                <div class="rd-navbar-nav-inner">
                                    <?= Menu::widget([
                                        'encodeLabels'=>false,
                                        'options'=>['class'=>'rd-navbar-nav'],
                                        'items' => [
                                            ['label' => 'Home', 'url' => ['/']],
                                            ['label' => 'About us', 'url' => '#'],
                                            ['label' => 'Services', 'url' => '#'],
                                            ['label' => 'Cases', 'url' => '#'],
                                            ['label' => 'Blog', 'url' => '#'],
                                            ['label' => 'Login', 'url' => ['/site/login']],
                                            ['label' => 'Contact us', 'url' => '#'],
                                            // 'Products' menu item will be selected as long as the route is 'product/index'
                                        ],
                                    ]); ?>

                                    <!--<ul class="rd-navbar-nav">
                                        <li class="active"><a href="index.html">Home</a></li>
                                        <li><a href="about.html">About us</a></li>
                                        <li><a href="services.html">Services</a></li>
                                        <li><a href="#">Cases</a></li>
                                        <li><a href="#">Blog</a></li>
                                        <li><a href="#">Login</a></li>
                                        <li><a href="contacts.html">Contact us</a>
                                        </li>
                                    </ul>-->
                                </div>
                            </div>
                            <div class="rd-navbar-search">
                                <form action="search-results.html" method="GET" data-search-live="rd-search-results-live" data-search-live-count="6" class="rd-search">
                                    <div class="rd-search-inner">
                                        <div class="form-group">
                                            <label for="rd-search-form-input" class="form-label">Search...</label>
                                            <input id="rd-search-form-input" type="text" name="s" autocomplete="off" class="form-control">
                                        </div>
                                        <button type="submit" class="rd-search-submit"></button>
                                    </div>
                                    <div id="rd-search-results-live" class="rd-search-results-live"></div>
                                </form>
                                <button data-rd-navbar-toggle=".rd-navbar-search, .rd-navbar-search-wrap" class="rd-navbar-search-toggle"></button>
                            </div>
                        </div>
                    </div>
                </div>
            </nav>
        </div>
    </header>

    <?= $content ?>

</div>
<div id="form-output-global" class="snackbars"></div>
<div tabindex="-1" role="dialog" aria-hidden="true" class="pswp">
    <div class="pswp__bg"></div>
    <div class="pswp__scroll-wrap">
        <div class="pswp__container">
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
        </div>
        <div class="pswp__ui pswp__ui--hidden">
            <div class="pswp__top-bar">
                <div class="pswp__counter"></div>
                <button title="Close (Esc)" class="pswp__button pswp__button--close"></button>
                <button title="Share" class="pswp__button pswp__button--share"></button>
                <button title="Toggle fullscreen" class="pswp__button pswp__button--fs"></button>
                <button title="Zoom in/out" class="pswp__button pswp__button--zoom"></button>
                <div class="pswp__preloader">
                    <div class="pswp__preloader__icn">
                        <div class="pswp__preloader__cut">
                            <div class="pswp__preloader__donut"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                <div class="pswp__share-tooltip"></div>
            </div>
            <button title="Previous (arrow left)" class="pswp__button pswp__button--arrow--left"></button>
            <button title="Next (arrow right)" class="pswp__button pswp__button--arrow--right"></button>
            <div class="pswp__caption">
                <div class="pswp__caption__cent"></div>
            </div>
        </div>
    </div>
</div>
<script data-cfasync="false" src="<?= Yii::$app->request->baseUrl; ?>/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script>
<script src="<?= Yii::$app->request->baseUrl; ?>/js/core.min.js"></script>
<script src="<?= Yii::$app->request->baseUrl; ?>/js/script.js"></script>
<script type="text/javascript">
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-7078796-5']);
    _gaq.push(['_trackPageview']);
    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();</script>
<?php $this->endBody() ?>
    </body>

    </html>
<?php $this->endPage() ?>