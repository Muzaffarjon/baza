<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\widgets\Menu;

//AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>

<html lang="<?= Yii::$app->language ?>" class="wide wow-animation">
<head>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <script src="<?= Yii::$app->request->baseUrl; ?>/cdn-cgi/apps/head/3ts2ksMwXvKRuG480KNifJ2_JNM.js"></script><link rel="icon" href="<?= Yii::$app->request->baseUrl; ?>/images/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Arvo:400,700%7COpen+Sans:300,300italic,400,400italic,700italic,800%7CUbuntu:500">
    <link rel="stylesheet" href="<?= Yii::$app->request->baseUrl; ?>/css/style.css">
    <script src="<?= Yii::$app->request->baseUrl; ?>/master/vendor/jQuery-2.1.4.min.js"></script>
    <!--[if lt IE 10]>
    <div style="background: #212121; padding: 10px 0; box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3); clear: both; text-align:center; position: relative; z-index:1;"><a href="httpqwertywindows.microsoft.com/en-US/internet-explorer/"><img src="images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    <script src="<?= Yii::$app->request->baseUrl; ?>/js/html5shiv.min.js"></script>
    <![endif]-->
    <?php $this->head() ?>
</head>
<body style="background-image: url(<?= Yii::$app->request->baseUrl; ?>/images/bg-login.jpg);" class="one-screen-page bg-gray-darker bg-image">
<?php $this->beginBody() ?>
<div class="page">
    <!--<div class="page-loader page-loader-variant-1">
        <div><a href="" class="brand brand-md brand-inverse"><img src="<?= Yii::$app->request->baseUrl; ?>/images/logo-light-145x30.png" alt="" width="145" height="30"/></a>
            <div class="page-loader-body">
                <div id="spinningSquaresG">
                    <div id="spinningSquaresG_1" class="spinningSquaresG"></div>
                    <div id="spinningSquaresG_2" class="spinningSquaresG"></div>
                    <div id="spinningSquaresG_3" class="spinningSquaresG"></div>
                    <div id="spinningSquaresG_4" class="spinningSquaresG"></div>
                    <div id="spinningSquaresG_5" class="spinningSquaresG"></div>
                    <div id="spinningSquaresG_6" class="spinningSquaresG"></div>
                    <div id="spinningSquaresG_7" class="spinningSquaresG"></div>
                    <div id="spinningSquaresG_8" class="spinningSquaresG"></div>
                </div>
            </div>
        </div>
    </div>-->
    <div class="page-inner">
        <header class="page-head">
            <div class="page-head-inner">
                <div class="shell text-center"><a href="index.html" class="brand brand-md brand-inverse"><img src="<?= Yii::$app->request->baseUrl; ?>/images/logo-light-145x30.png" alt="" width="145" height="30"/></a>
                </div>
            </div>
        </header>
        <section>
            <?= $content; ?>
        </section>
        <section class="page-foot">
            <div class="page-foot-inner">
                <div class="shell text-center">
                    <div class="range">
                        <div class="cell-xs-12">
                            <p class="rights"><span>Starbis</span><span>&nbsp;&#169;&nbsp;</span><span id="copyright-year"></span><span>All Rights Reserved</span><br class="veil-sm"><a href="#" class="link-primary-inverse">Terms of Use</a><span>and</span><a href="privacy-policy.html" class="link-primary-inverse">Privacy Policy</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </div>
</div>
<div id="form-output-global" class="snackbars"></div>
<div tabindex="-1" role="dialog" aria-hidden="true" class="pswp">
    <div class="pswp__bg"></div>
    <div class="pswp__scroll-wrap">
        <div class="pswp__container">
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
        </div>
        <div class="pswp__ui pswp__ui--hidden">
            <div class="pswp__top-bar">
                <div class="pswp__counter"></div>
                <button title="Close (Esc)" class="pswp__button pswp__button--close"></button>
                <button title="Share" class="pswp__button pswp__button--share"></button>
                <button title="Toggle fullscreen" class="pswp__button pswp__button--fs"></button>
                <button title="Zoom in/out" class="pswp__button pswp__button--zoom"></button>
                <div class="pswp__preloader">
                    <div class="pswp__preloader__icn">
                        <div class="pswp__preloader__cut">
                            <div class="pswp__preloader__donut"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                <div class="pswp__share-tooltip"></div>
            </div>
            <button title="Previous (arrow left)" class="pswp__button pswp__button--arrow--left"></button>
            <button title="Next (arrow right)" class="pswp__button pswp__button--arrow--right"></button>
            <div class="pswp__caption">
                <div class="pswp__caption__cent"></div>
            </div>
        </div>
    </div>
</div>
<script data-cfasync="false" src="<?= Yii::$app->request->baseUrl; ?>/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script>
<!--<script src="--><?//= Yii::$app->request->baseUrl; ?><!--/js/core.min.js"></script>-->
<!--<script src="--><?//= Yii::$app->request->baseUrl; ?><!--/js/script.js"></script>-->

<?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>