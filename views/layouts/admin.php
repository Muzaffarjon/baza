<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\widgets\Menu;
use app\models\Control;
use yii\helpers\Url;
//AppAsset::register($this);
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <!-- Required meta tags-->
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="au theme template">
        <meta name="author" content="Hau Nguyen">
        <meta name="keywords" content="au theme template">

        <!-- Title Page-->
        <title><?= Html::encode($this->title) ?></title>

        <!-- Fontfaces CSS-->
        <link href="<?= Yii::$app->request->baseUrl; ?>/master/css/font-face.css" rel="stylesheet" media="all">
        <link href="<?= Yii::$app->request->baseUrl; ?>/master/vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
        <link href="<?= Yii::$app->request->baseUrl; ?>/master/vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
        <link href="<?= Yii::$app->request->baseUrl; ?>/master/vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

        <!-- Bootstrap CSS-->
        <link href="<?= Yii::$app->request->baseUrl; ?>/master/vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

        <!-- Vendor CSS-->
        <link href="<?= Yii::$app->request->baseUrl; ?>/master/vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
        <link href="<?= Yii::$app->request->baseUrl; ?>/master/vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
        <link href="<?= Yii::$app->request->baseUrl; ?>/master/vendor/wow/animate.css" rel="stylesheet" media="all">
        <link href="<?= Yii::$app->request->baseUrl; ?>/master/vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
        <link href="<?= Yii::$app->request->baseUrl; ?>/master/vendor/slick/slick.css" rel="stylesheet" media="all">
        <link href="<?= Yii::$app->request->baseUrl; ?>/master/vendor/select2/select2.min.css" rel="stylesheet" media="all">
        <link href="<?= Yii::$app->request->baseUrl; ?>/master/vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">
        <link href="<?= Yii::$app->request->baseUrl; ?>/master/vendor/vector-map/jqvmap.min.css" rel="stylesheet" media="all">
        <script src="<?= Yii::$app->request->baseUrl; ?>/master/vendor/jQuery-2.1.4.min.js"></script>
        <!-- Main CSS-->
        <link href="<?= Yii::$app->request->baseUrl; ?>/master/css/theme.css" rel="stylesheet" media="all">
        <?= Html::csrfMetaTags() ?>
        <style>
            .disabled{
                opacity: 0.4;
                pointer-events: none;
            }
        </style>
    </head>

    <?php
    $url=Control::getUrl(Yii::$app->request->url);
    //var_dump($url);
    if($url===Url::to(['users/reports'])){
        $class="";
    } else $class="animsition";
    ?>
    <body class="<?= $class ?>">
    <div class="page-wrapper">
        <!-- MENU SIDEBAR-->
        <aside class="menu-sidebar2">
            <div class="logo">
                <a href="#">
                    <img src="<?= Yii::$app->request->baseUrl; ?>/master/images/icon/logo-white.png" alt="Cool Admin" />
                </a>
            </div>
            <div class="menu-sidebar2__content js-scrollbar1">
                <div class="account2">
                    <div class="image img-cir img-120">
                        <img src="<?= Yii::$app->request->baseUrl; ?>/master/images/icon/avatar-big-01.jpg" alt="John Doe" />
                    </div>
                    <h4 class="name"><?= Yii::$app->user->identity->getFullname(); ?></h4>
                    <a href="#"><?= \app\models\Control::getUserType(Yii::$app->user->identity->role); ?></a>
                    <a href="#"><?=
                        Html::beginForm(['/site/logout'], 'post').
                        Html::submitButton(
                            'Выйти из аккаунта',
                            ['class' => 'logout']
                        )
                        . Html::endForm();

                        ?></a>
                </div>
                <nav class="navbar-sidebar2">
                    <?= Yii::$app->controller->renderPartial('/layouts/extra_admin/_menu') ?>
                </nav>
            </div>
        </aside>
        <!-- END MENU SIDEBAR-->

        <!-- PAGE CONTAINER-->
        <div class="page-container2">
            <!-- HEADER DESKTOP-->
            <header class="header-desktop2">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="header-wrap2">
                            <div class="logo d-block d-lg-none">
                                <a href="#">
                                    <img src="<?= Yii::$app->request->baseUrl; ?>/master/images/icon/logo-white.png" alt="CoolAdmin" />
                                </a>
                            </div>
                            <div class="header-button2">

                                <div class="header-button-item mr-0 js-sidebar-btn">
                                    <i class="zmdi zmdi-menu"></i>
                                </div>
                                <div class="setting-menu js-right-sidebar d-none d-lg-block">
                                    <div class="account-dropdown__body">
                                        <div class="account-dropdown__item">
                                            <a href="#">
                                                <i class="zmdi zmdi-account"></i>Account</a>
                                        </div>
                                        <div class="account-dropdown__item">
                                            <a href="#">
                                                <i class="zmdi zmdi-settings"></i>Setting</a>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <aside class="menu-sidebar2 js-right-sidebar d-block d-lg-none">
                <div class="logo">
                    <a href="#">
                        <img src="<?= Yii::$app->request->baseUrl; ?>/master/images/icon/logo-white.png" alt="Cool Admin" />
                    </a>
                </div>
                <div class="menu-sidebar2__content js-scrollbar2">
                    <div class="account2">
                        <div class="image img-cir img-120">
                            <img src="<?= Yii::$app->request->baseUrl; ?>/master/images/icon/avatar-big-01.jpg" alt="John Doe" />
                        </div>
                        <h4 class="name"><?= Yii::$app->user->identity->getFullname(); ?></h4>
                        <a href="#"><?= \app\models\Control::getUserType(Yii::$app->user->identity->role); ?></a>
                        <a href="#"><?=
                            Html::beginForm(['/site/logout'], 'post').
                            Html::submitButton(
                                'Выйти из аккаунта',
                                ['class' => 'logout']
                            )
                            . Html::endForm();

                            ?></a>
                    </div>  
                    <nav class="navbar-sidebar2">
                        <?= Yii::$app->controller->renderPartial('/layouts/extra_admin/_menu') ?>
                    </nav>
                </div>
            </aside>
            <!-- END HEADER DESKTOP-->

            <!-- BREADCRUMB-->
            <section class="au-breadcrumb m-t-75">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="au-breadcrumb-content">
                                    <div class="au-breadcrumb-left">
                                        <span class="au-breadcrumb-span">Сейчас вы находитесь здесь:</span>
                                        <?php
                                        echo Breadcrumbs::widget([
                                            'options' => [
                                                'class' => 'list-unstyled list-inline au-breadcrumb__list',
                                            ],
                                            //'separator'=>'<span>/</span>',
                                            'itemTemplate' => "<li class='list-inline-item'>{link}</li>",
                                            'homeLink' => [
                                                'label' => 'Главная',
                                                'url' => ['users/account'],
                                            ],
                                            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [
                                            ],
                                        ]);  ?>
                                        <!--<ul class="list-unstyled list-inline au-breadcrumb__list">
                                            <li class="list-inline-item active">
                                                <a href="#">Home</a>
                                            </li>
                                            <li class="list-inline-item seprate">
                                                <span>/</span>
                                            </li>
                                            <li class="list-inline-item">Dashboard</li>
                                        </ul>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- END BREADCRUMB-->
            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content">
                    <div class="container-fluid">
                        <?= $content; ?>
                    </div>
                </div>
            </div>


            <!-- END PAGE CONTAINER-->
        </div>

    </div>
    <div class="modal fade" id="smallmodal" tabindex="-1" role="dialog" aria-labelledby="smallmodalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="smallmodalLabel">Small Modal</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>
                        There are three species of zebras: the plains zebra, the mountain zebra and the Grévy's zebra. The plains zebra and the mountain
                        zebra belong to the subgenus Hippotigris, but Grévy's zebra is the sole species of subgenus Dolichohippus. The latter
                        resembles an ass, to which it is closely related, while the former two are more horse-like. All three belong to the
                        genus Equus, along with other living equids.
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary">Confirm</button>
                </div>
            </div>
        </div>
    </div>
<script>
    $(document).on('pjax:send', function() {
        $('#reload').addClass('disabled');
    })
    $(document).on('pjax:complete', function() {
        $('#reload').removeClass('disabled');
    })
</script>
    <!-- Jquery JS-->
<!--    <script src="--><?//= Yii::$app->request->baseUrl; ?><!--/master/vendor/jquery-3.2.1.min.js"></script>-->
    <!-- Bootstrap JS-->
    <script src="<?= Yii::$app->request->baseUrl; ?>/master/vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="<?= Yii::$app->request->baseUrl; ?>/master/vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="<?= Yii::$app->request->baseUrl; ?>/master/vendor/slick/slick.min.js">
    </script>
    <script src="<?= Yii::$app->request->baseUrl; ?>/master/vendor/wow/wow.min.js"></script>
    <script src="<?= Yii::$app->request->baseUrl; ?>/master/vendor/animsition/animsition.min.js"></script>
    <script src="<?= Yii::$app->request->baseUrl; ?>/master/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="<?= Yii::$app->request->baseUrl; ?>/master/vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="<?= Yii::$app->request->baseUrl; ?>/master/vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="<?= Yii::$app->request->baseUrl; ?>/master/vendor/circle-progress/circle-progress.min.js"></script>
    <script src="<?= Yii::$app->request->baseUrl; ?>/master/vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="<?= Yii::$app->request->baseUrl; ?>/master/vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="<?= Yii::$app->request->baseUrl; ?>/master/vendor/select2/select2.min.js">
    </script>
    <script src="<?= Yii::$app->request->baseUrl; ?>/master/vendor/vector-map/jquery.vmap.js"></script>
    <script src="<?= Yii::$app->request->baseUrl; ?>/master/vendor/vector-map/jquery.vmap.min.js"></script>
    <script src="<?= Yii::$app->request->baseUrl; ?>/master/vendor/vector-map/jquery.vmap.sampledata.js"></script>
    <script src="<?= Yii::$app->request->baseUrl; ?>/master/vendor/vector-map/jquery.vmap.world.js"></script>

    <!-- Main JS-->
    <script src="<?= Yii::$app->request->baseUrl; ?>/master/js/main.js"></script>

    </body>

    </html>
    <!-- end document-->


<?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>