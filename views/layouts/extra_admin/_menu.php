<?php
use yii\widgets\Menu;
use app\models\Control;
$menu=Control::getMenuList(\Yii::$app->user->identity->role);
?>
<?= Menu::widget([
        'encodeLabels'=>false,
        'options'=>['class'=>'list-unstyled navbar__list'],
        'items' => $menu
        // Important: you need to specify url as 'controller/action',
        // not just as 'controller' even if default action is used.
        //['label' => '<i class="fas fa-tachometer-alt"></i>Dashboard', 'url' => ['admin/index']],
        //['label' => '<i class="fas  fa-users"></i>Designers', 'url' => ['admin/designers']],
        //['label' => '<i class="fas  fa-user"></i>All Users', 'url' => ['users/index']],
        //['label' => '<i class="fas  fa-flag"></i>Slider', 'url' => ['admin/sliders']],
        /*Yii::$app->user->isGuest ? (
        ""
        ) : (['label'=>Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                'Logout (' . Yii::$app->user->identity->username . ')',
                ['class' => 'logout']
            )
            . Html::endForm()
        ]
        )*/
        // 'Products' menu item will be selected as long as the route is 'product/index'

    ]); ?>
