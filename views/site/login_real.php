<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Вход';
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="shell">
    <div class="range range-sm-center">
        <div class="cell-sm-7 cell-md-5 cell-lg-4">
            <div class="block-shadow text-center">
                <div class="block-inner">
                    <p class="text-uppercase text-bold text-dark">Login to your account</p>
                    <div class="offset-top-40 offset-sm-top-60"><span class="icon icon-xl icon-gray-base material-icons-lock_open"></span></div>
                </div>
                <?php $form = ActiveForm::begin([
                    'id' => 'login-form',
                    'enableClientValidation'=>true,
                    'enableAjaxValidation'=>false,
                    'options'=>[
                    'class'=>'rd-mailform form-modern form-darker offset-top-40'
                        ],
                ]); ?>
                <div class="block-inner">
                <?= $form->field($model, 'username')->textInput(['autofocus' => true,'placeholder'=>'Login'])->label(false) ?>

                <?= $form->field($model, 'password')->passwordInput(['placeholder'=>'Password'])->label(false) ?>
                </div>
                <div class="offset-top-30 offset-sm-top-60">
                    <?= Html::submitButton('Login', ['class' => 'btn btn-rect btn-primary btn-block', 'name' => 'login-button']) ?>
                </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>
    </div>
</div>