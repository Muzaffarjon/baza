<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>

<div class="page-inner">
    <header class="page-head">
        <div class="page-head-inner">
            <div class="shell text-center"><a href="index.html" class="brand brand-md brand-inverse"><img src="<?= Yii::$app->request->baseUrl; ?>/images/logo-light-145x30.png" alt="" width="145" height="30"/></a>
            </div>
        </div>
    </header>
    <section>
        <section class="text-center">
            <div class="shell">
                <div class="range range-sm-center">
                    <div class="cell-sm-9 cell-md-8">
                        <h5>Sorry, but page was not found</h5>
                        <div class="text-extra-large-bordered offset-top-15">
                            <p>404</p>
                        </div>
                        <p class="text-white"><?= nl2br(Html::encode($message)) ?></p>
                        <div class="group-xl offset-top-40 offset-sm-top-60 offset-xl-top-120"><a href="<?= Yii::$app->homeUrl; ?>" class="btn btn-rect btn-primary">Back to home</a><a href="#" class="btn btn-rect btn-white-outline">contact us</a></div>
                    </div>
                </div>
            </div>
        </section>
    </section>
    <section class="page-foot">
        <div class="page-foot-inner">
            <div class="shell text-center">
                <div class="range">
                    <div class="cell-xs-12">
                        <p class="rights"><span>Starbis</span><span>&nbsp;&#169;&nbsp;</span><span id="copyright-year"></span><span>All Rights Reserved</span><br class="veil-sm"><a href="#" class="link-primary-inverse">Terms of Use</a><span>and</span><a href="privacy-policy.html" class="link-primary-inverse">Privacy Policy</a></p>
                    </div>
                </div>
            </div>
        </div>
    </section>

</div>